import React, { Component } from 'react'
import { Row, Col } from 'antd'
import { connect } from 'react-redux'
import { PathTitle } from '../../components/pathTitle'
import { TableReport } from '../../components/tableReport'
import { Service as ServiceMember } from '../../services/member'
import { numberWithCommas } from '../../lib/utils' 
import { Service } from '../../services/report' 
import moment from 'moment'
import { DatePicker } from 'antd'
import 'moment/locale/th';

const { RangePicker } = DatePicker;

const data = [
    {
        account : 'user_acount',
        contact : 'contact',
        currency : 'THB',
        revolving_money : numberWithCommas(5000), 
        revolving_real_money : numberWithCommas(5000),
        member_unit : numberWithCommas(100),
        count_invest : numberWithCommas(685)
    }
];

function TitleUsername(props){
    return (
        <div>
            <span style={{fontSize:12,color:'rgb(112, 112, 112)'}}>ชื่อ : </span><span style={{paddingTop:'50%',fontSize:13,fontWeight:'900',color:'rgb(249, 139, 139)'}}>{props.name}</span>
        </div>
    )
}

function FilterDiv(props){
    return (
        <div style={{textAlign:'center'}}>
            <div style={{paddingLeft:8,paddingRight:8,fontSize:12,color:'rgb(112, 112, 112)'}}>
                {props.children}
            </div>
        </div>
    )
}   


class Rpt_win_lose extends Component {

    constructor(props) {
        super(props)

        this.state = {
            type:undefined,
            dateStart:null,
            dateFinish:null,
            dataLists:[],
            columns:{
                supersenior:[
                    {
                        title: "บัญชี",
                        dataIndex: "member_id",
                        key:"member_id",
                        align:'center',
                    },
                    {
                        title: "ติดต่อ",
                        dataIndex: "member_name",
                        key:"member_name",
                        align:'center'
                    },
                    {
                        title: "เงินหมุนเวียน",
                        dataIndex: "revolving_money",
                        key:"revolving_money",
                        align:'center'
                    },
                    {
                        title: "เครดิตปัจจุบัน",
                        dataIndex: "revolving_real_money",
                        key:"revolving_real_money",
                        align:'center'
                    },
                    {
                        title: "การนับเดิมพัน",
                        dataIndex: "count_invest",
                        key:"count_invest",
                        align:'center'
                    },
                    {
                        title: "ซีเนียร์",
                        dataIndex: "credit_share_senior",
                        align:'center',
                        key:"credit_share_senior",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    },
                    {
                        title: "บริษัท",
                        dataIndex: "credit_share_supersenior",
                        key:"credit_share_supersenior",
                        align:'center',
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    }
                ],
                senior:[
                    {
                        title: "บัญชี",
                        dataIndex: "member_id",
                        key:"member_id",
                        align:'center',
                    },
                    {
                        title: "ติดต่อ",
                        dataIndex: "member_name",
                        key:"member_name",
                        align:'center'
                    },
                    {
                        title: "เงินหมุนเวียน",
                        dataIndex: "revolving_money",
                        key:"revolving_money",
                        align:'center'
                    },
                    {
                        title: "เครดิตปัจจุบัน",
                        dataIndex: "revolving_real_money",
                        key:"revolving_real_money",
                        align:'center'
                    },
                    {
                        title: "การนับเดิมพัน",
                        dataIndex: "count_invest",
                        key:"count_invest",
                        align:'center'
                    },
                    {
                        title: "สมาชิก",
                        dataIndex: "member_total_win_lose",
                        align:'center',
                        key:"member_total_win_lose",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    },
                    {
                        title: "เอเย่น",
                        dataIndex: "credit_share_agent",
                        align:'center',
                        key:"credit_share_agent",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    },
                    {
                        title: "มาสเตอร์",
                        dataIndex: "credit_share_master",
                        align:'center',
                        key:"credit_share_master",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    },
                    {
                        title: "ซีเนียร์",
                        dataIndex: "credit_share_senior",
                        align:'center',
                        key:"credit_share_senior",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    },
                    {
                        title: "บริษัท",
                        dataIndex: "credit_share_supersenior",
                        key:"credit_share_supersenior",
                        align:'center',
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    }
                ],
                master:[
                    {
                        title: "บัญชี",
                        dataIndex: "member_id",
                        key:"member_id",
                        align:'center'
                    },
                    {
                        title: "ติดต่อ",
                        dataIndex: "member_name",
                        key:"member_name",
                        align:'center'
                    },
                    {
                        title: "เงินหมุนเวียน",
                        dataIndex: "revolving_money",
                        key:"revolving_money",
                        align:'center'
                    },
                    {
                        title: "เครดิตปัจจุบัน",
                        dataIndex: "revolving_real_money",
                        key:"revolving_real_money",
                        align:'center'
                    },
                    {
                        title: "การนับเดิมพัน",
                        dataIndex: "count_invest",
                        key:"count_invest",
                        align:'center'
                    },
                    {
                        title: "สมาชิก",
                        dataIndex: "member_total_win_lose",
                        align:'center',
                        key:"member_total_win_lose",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    },
                    {
                        title: "เอเย่น",
                        dataIndex: "credit_share_agent",
                        align:'center',
                        key:"credit_share_agent",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    },
                    {
                        title: "มาสเตอร์",
                        dataIndex: "credit_share_master",
                        align:'center',
                        key:"credit_share_master",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    },
                    {
                        title: "ซีเนียร์",
                        dataIndex: "credit_share_senior",
                        align:'center',
                        key:"credit_share_senior",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    }
                ],
                agent:[
                    {
                        title: "บัญชี",
                        dataIndex: "member_id",
                        key:"member_id",
                        align:'center'
                    },
                    {
                        title: "ติดต่อ",
                        dataIndex: "member_name",
                        key:"member_name",
                        align:'center'
                    },
                    {
                        title: "เงินหมุนเวียน",
                        dataIndex: "revolving_money",
                        key:"revolving_money",
                        align:'center'
                    },
                    {
                        title: "เครดิตปัจจุบัน",
                        dataIndex: "revolving_real_money",
                        key:"revolving_real_money",
                        align:'center'
                    },
                    {
                        title: "การนับเดิมพัน",
                        dataIndex: "count_invest",
                        key:"count_invest",
                        align:'center'
                    },
                    {
                        title: "สมาชิก",
                        dataIndex: "member_total_win_lose",
                        align:'center',
                        key:"member_total_win_lose",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    },
                    {
                        title: "เอเย่น",
                        dataIndex: "credit_share_agent",
                        align:'center',
                        key:"credit_share_agent",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    },
                    {
                        title: "มาสเตอร์",
                        dataIndex: "credit_share_master",
                        align:'center',
                        key:"credit_share_master",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    }
                ],
                member:[
                    {
                        title: "บัญชี",
                        dataIndex: "member_id",
                        key:"member_id",
                        align:'center'
                    },
                    {
                        title: "ติดต่อ",
                        dataIndex: "member_name",
                        key:"member_name",
                        align:'center'
                    },
                    {
                        title: "เงินหมุนเวียน",
                        dataIndex: "revolving_money",
                        key:"revolving_money",
                        align:'center'
                    },
                    {
                        title: "เครดิตปัจจุบัน",
                        dataIndex: "revolving_real_money",
                        key:"revolving_real_money",
                        align:'center'
                    },
                    {
                        title: "การนับเดิมพัน",
                        dataIndex: "count_invest",
                        key:"count_invest",
                        align:'center'
                    },
                    {
                        title: "สมาชิก",
                        dataIndex: "member_total_win_lose",
                        align:'center',
                        key:"member_total_win_lose",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    },
                    {
                        title: "เอเย่น",
                        dataIndex: "credit_share_agent",
                        align:'center',
                        key:"credit_share_agent",
                        render:(record)=>{
                            return this.renderColumn(record)
                        }
                    }
                ]
            },
            parent:{
                token:undefined,
                id:undefined
            }
        }
    }

    componentWillMount(){
        this.setState({dateStart: moment((new Date())).format('YYYY-MM-DD') , dateFinish: moment((new Date())).format('YYYY-MM-DD') })
    }


    getPriolityById = (token,_id) => {
        return new Promise((resolve, reject) => {
          ServiceMember.getById(token,_id).then((res)=>{
              if(res.status){
                resolve({status:true,data : res.data.data})
              }else{
                resolve({status:false})
              }
          })
        }); 
    }

    renderColumn = (_data) => {
        const number = parseInt(_data)
        if(number < 0){
            return <span style={{color:'#FF5733'}}>{numberWithCommas(_data)}</span>
        }
        else if(number === 0){
            return <span style={{color:'#000000'}}>{numberWithCommas(_data)}</span>
        }
        else{
            return <span style={{color:'#1FDE7A'}}>{numberWithCommas(_data)}</span>
        }
    }

    onDatePickerChange = ( date, dateString, dateType ) => {

        const newDate =  new Date(dateString)

        if(dateType === 'start'){
            this.setState({
                dateStart:dateString,
            })
        }else{
            const dateStart = new Date(this.state.dateStart)
            if(newDate.getTime(dateString) < dateStart){
                alert("วันที่สิ้นสุดต้องเป็นวันที่ หลังจากวันที่เริ่มต้น")
            }else{
                this.setState({
                    dateFinish:dateString
                })
            }
        }

    }

    handleSearchMode = (_type) => {
        if(this.props.special_priority.priority === 'watcher'){
            this.getPriolityById(this.props.auth.token,this.props.special_priority.parentId).then((res1)=>{
                if(res1.status){
                    Service.getReportWinAndLose(res1.data.tokens[0].token,{type:_type}).then((res,err) => {
                        if(res.status){
                            this.setState({type:_type})
                            this.formatObject(res.data.data)
                        }
                    })
                }
            })
        }else{
            Service.getReportWinAndLose(this.props.auth.token,{type:_type}).then((res,err) => {
                if(res.status){
                    this.setState({type:_type})
                    this.formatObject(res.data.data)
                }
            })
        }
    }

    funcChangeValue = (_bool,_number) => {
        if( _bool ){
            return Math.abs(_number)
        }else{
            return _number - (_number * 2)
        }
    }

    formatObject = async (_data) => {
       
        if(_data.length > 0){
            
            let member_total_win_lose = 0
            let credit_share_agent = 0
            let credit_share_master = 0
            let credit_share_senior = 0
            let credit_share_supersenior = 0

            let results = await  _data.map((row,index) => {

                let isMinus = false

                if(row.total.totalSum < 0 ){
                    isMinus = true
                }

                member_total_win_lose += parseFloat(row.total.totalSum)
                credit_share_agent += parseFloat(row.share.credit_share_agent)
                credit_share_master += parseFloat(row.share.credit_share_master)
                credit_share_senior += this.funcChangeValue(isMinus,parseFloat((row.share.credit_share_senior.toFixed(2))))
                credit_share_supersenior += this.funcChangeValue(isMinus,parseFloat((row.share.credit_share_supersenior.toFixed(2))))

                return {
                    member_id : row.member.member_id,
                    member_name : row.member.member_name,
                    revolving_money : numberWithCommas(parseFloat((row.total.turnOver.toFixed(2)))),
                    revolving_real_money : numberWithCommas(parseFloat((row.member.member_credit.toFixed(2)))),
                    count_invest : numberWithCommas(row.total.totalMatch),
                    member_total_win_lose : numberWithCommas(parseFloat((row.total.totalSum.toFixed(2)))),
                    credit_share_agent : this.funcChangeValue(isMinus,parseFloat((row.share.credit_share_agent.toFixed(2)))),
                    credit_share_master : this.funcChangeValue(isMinus,parseFloat((row.share.credit_share_master.toFixed(2)))),
                    credit_share_senior : this.funcChangeValue(isMinus,parseFloat((row.share.credit_share_senior.toFixed(2)))),
                    credit_share_supersenior : this.funcChangeValue(isMinus,parseFloat((row.share.credit_share_supersenior.toFixed(2))))
                }

            })

            results.push({
                member_id : 'รวม',
                member_name : '',
                revolving_money : '',
                revolving_real_money : '',
                count_invest : '',
                member_total_win_lose : parseFloat((member_total_win_lose.toFixed(2))),
                credit_share_agent : parseFloat((credit_share_agent.toFixed(2))),
                credit_share_master : parseFloat((credit_share_master.toFixed(2))),
                credit_share_senior : parseFloat((credit_share_senior.toFixed(2))),
                credit_share_supersenior : parseFloat((credit_share_supersenior.toFixed(2)))
            })
            const NewResults = results
            await this.setState({dataLists:NewResults})
        }
           
        
    }

    handleSearch = () => {

        if(this.state.dateStart === null ){
            alert('กรุณากรอกเลือกวันที่เริ่มต้นด้วยค่ะ')
        }
        else if(this.state.dateFinish === null ){
            alert('กรุณากรอกเลือกวันที่สิ้นสุดต้นด้วยค่ะ')
        }
        // else if(){

        // }
        else{

            let obj = {
                type:'between',
                from:this.state.dateStart,
                to:this.state.dateFinish
            }

            if(this.props.special_priority.priority === 'watcher'){
                this.getPriolityById(this.props.auth.token,this.props.special_priority.parentId).then((res)=>{
                    if(res.status){
                        Service.getReportWinAndLose(res.data.tokens[0].token,obj).then((res,err) => {
                            if(res.status){
                                this.setState({type:'between'})
                                this.formatObject(res.data.data)
                            }
                        })
                    }
                })
            }else{
                Service.getReportWinAndLose(this.props.auth.token,obj).then((res,err) => {
                    if(res.status){
                        this.setState({type:'between'})
                        this.formatObject(res.data.data)
                    }
                })
            }
    
        }

    }

    renderTypeSearch = (_tyle) => {

        if(_tyle === 'day'){
            return 'ข้อมูลรายงาน วันนี้'
        }
        else if(_tyle === 'week'){
            return 'ข้อมูลรายงาน สัปดาห์นี้'
        }
        else if(_tyle === 'yesterday'){
            return 'ข้อมูลรายงาน เมื่อวานนี้'
        }else{
            if(this.state.dateStart === null || this.state.dateFinish === null){
                return null
            }else{
                return  <p> ข้อมูลรายงาน จากวันที่ &nbsp;&nbsp; {this.state.dateStart}  &nbsp;&nbsp; ถึง &nbsp;&nbsp;  {this.state.dateFinish} </p>
            }
            
        }
    }

    render(){

        const { special_priority , priority , auth } = this.props
        const { type , columns , dataLists , dateStart , dateFinish } = this.state
        
        return (
            <div>
                <Row>
                    <Col span={24}>
                        <PathTitle textPath="รายงาน -  แพ้ ชนะ (รายละเอียด)" />
                    </Col>
                </Row>
                <div style={{height:10}}></div>
                <Row>
                    <Col xs={24} md={5}>
                        <TitleUsername name={(auth.member === null ? null : auth.member.member_name )} /><br/>
                    </Col>
                    <Col xs={24} md={9}>
                        <Row>
                            <Col xs={12}>
                                <DatePicker value={moment(dateStart,'YYYY-MM-DD')} placeholder="เลือกวันเริ่มต้น" onChange={( date , dateString )=>{this.onDatePickerChange(date,dateString,'start')}} />   
                            </Col>
                            <Col xs={12}>
                                <DatePicker value={moment(dateFinish,'YYYY-MM-DD')} placeholder="เลือกวันสิ้นสุด" onChange={( date , dateString )=>{this.onDatePickerChange(date,dateString,'end')}} />   
                            </Col>
                        </Row>
                    </Col>
                    <Col xs={24} md={10}>
                        <FilterDiv>
                            <button key="search" style={{margin:2,fontSize:15,borderRadius:4,height:32,backgroundColor:'#f4f4f4',cursor:'pointer',minWidth:80,borderColor:'rgb(249, 139, 139)',color:'rgb(249, 139, 139)'}} onClick={()=>{this.handleSearch()}} >
                                ค้นหา
                            </button>
                            <button key="btnYesterday" style={{margin:2,fontSize:15,borderRadius:4,height:32,backgroundColor:'#f4f4f4',cursor:'pointer',minWidth:80,borderColor:'rgb(104, 173, 232)',color:'rgb(104, 173, 232)'}} onClick={()=>{this.handleSearchMode('yesterday')}} >
                                เมื่อวาน
                            </button>
                            <button key="btnWeekly" style={{margin:2,fontSize:15,borderRadius:4,height:32,backgroundColor:'#f4f4f4',cursor:'pointer',minWidth:80,borderColor:'rgb(104, 173, 232)',color:'rgb(104, 173, 232)'}} onClick={()=>{this.handleSearchMode('week')}} >
                                สัปดาห์นี้
                            </button>
                            <button key="btnDaily" style={{margin:2,fontSize:15,borderRadius:4,height:32,backgroundColor:'#f4f4f4',cursor:'pointer',minWidth:80,borderColor:'rgb(104, 173, 232)',color:'rgb(104, 173, 232)'}} onClick={()=>{this.handleSearchMode('day')}} >
                                วันนี้
                            </button>
                        </FilterDiv>
                    </Col>
                </Row>
                <div style={{height:10}}></div>
                <Row>
                    <Col span={24}>
                        <br/>
                        {this.renderTypeSearch(type)}
                    </Col>
                    <Col span={24}>
                        <TableReport columns={columns[priority]} dataList={dataLists} />
                    </Col>
                </Row>
            </div>
        )
    }

}

const mapStateToProps = state => {
    return { auth: state.auth }
}

export default connect( mapStateToProps )(Rpt_win_lose)
