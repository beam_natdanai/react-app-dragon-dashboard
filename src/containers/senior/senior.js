import React, { Component } from "react"
import { connect } from 'react-redux'
import { Row , Col} from 'antd'
import { Button } from 'antd'
import { Table } from 'antd'
import { Modal } from 'antd'
import { Spin } from 'antd'
import { titleName } from '../../data_share/all'
import PathTitle from '../../components/pathTitle/pathTitle'
import { FormManager } from '../../components/formManager'
import { FormMember } from '../../components/formMember'

import './senior.css'

class Senior extends Component {

    constructor(props) {
        super(props)
        this.state = {
            table: {
                fetch: false,
                list: [],
                columns: [
                    {
                        title: "ลำดับ",
                        dataIndex: "index",
                        align:'center',
                        render : (text, record, index) => (index + 1)
                    },
                    {
                        title: "รหัสผู้ใช้",
                        dataIndex: "id",
                        align:'center'
                    },
                    {
                        title: "ติดต่อ",
                        dataIndex: "name",
                        align:'center'
                    },
                    {
                        title: "เครดิตจำกัด",
                        dataIndex: "credit_limit",
                        align:'center'
                    },
                    {
                        title: "ตั้งค่า",
                        dataIndex: "setting",
                        render: (text, record, index) => <a disabled={(this.isWatcher(this.props.special_priority.priority))} onClick={()=>{this.handleClickEdit(record)}} > แก้ไข </a>,
                        align:'center'
                    },
                    {
                        title: "วันที่เพิ่ม",
                        dataIndex: "create_date",
                        align:'center'
                    },
                    {
                        title: "ล็อค",
                        dataIndex: "status",
                        render: (text, record, index) => {
                                return(
                                    <div>{(record.status === 'open' ? 
                                        <p style={{color:'#31B42F'}}>{record.status}</p>
                                    : 
                                        <p style={{color:'#F05B3B'}} >{record.status}</p>
                                    )}</div>
                                )
                        },
                        align:'center'
                    },
                    {
                        title: "เบิกเครดิตออก",
                        dataIndex: "pay",
                        render: (text, record, index) => {
                            return(
                                <button disabled={(this.isWatcher(this.props.special_priority.priority))} key="payCredit" style={{color:'#c43410',fontSize:13,borderRadius:3,backgroundColor:'#f4f4f4',cursor:'pointer',minWidth:60}} onClick={()=>{this.handleClickPayCredit(record)}} >
                                        เบิกเครดิต
                                </button>
                            )
                        },
                        align:'center'
                    },
                    {
                        title: "เติมเครดิต",
                        dataIndex: "topup",
                        render: (text, record, index) => {
                            return(
                                <button disabled={(this.isWatcher(this.props.special_priority.priority))} key="topupCredit" style={{color:'#5694C7',fontSize:13,borderRadius:3,backgroundColor:'#f4f4f4',cursor:'pointer',minWidth:60}} onClick={()=>{this.handleClickTopupCredit(record)}} >
                                        เติมเครดิต
                                </button>
                            )
                        },
                        align:'center'
                    },
                    {
                        title: "แก้ไขรหัสผ่าน",
                        dataIndex: "edit_pass",
                        render: (text, record, index) => {
                            return(
                            <button disabled={(this.isWatcher(this.props.special_priority.priority))} key="btnEditPass" style={{color:'#ffc235',fontSize:13,borderRadius:3,backgroundColor:'#f4f4f4',cursor:'pointer',minWidth:60}} onClick={()=>{this.handleClickEditPass(record)}} >
                                    แก้ไข
                            </button>
                            )
                        },
                        align:'center'
                    }
                ]
            },
            formAdd: {
                field: {
                    member_id: null,
                    member_pass: null,
                    member_credit:0,
                    member_name: null,
                    member_status: 'open',
                    member_priority: 'senior',
                    member_choice_pay: 'Daily',
                    member_promise_pay: [],
                    percent: 0
                },
                visible: false,
                loading: false,
                dataComponent: {
                    payChoiceAllState:{
                        Power:'Power',
                        Daily:'Daily',
                        Weekly:'Weekly'
                    },
                }
            },
            formEdit: {
                field: {
                    id: null,
                    member_id: null,
                    member_credit: 0,
                    member_name: null,
                    member_choice_pay: null,
                    member_promise_pay: [],
                    percent: 0,
                    member_status: null
                },
                visible: false,
                loading: false,
                dataComponent: {
                    payChoiceAllState:{
                        Power:'Power',
                        Daily:'Daily',
                        Weekly:'Weekly'
                    },
                }
            }
        }
    }

    componentDidMount() {

    }

    componentDidUpdate(preProps){

    }

    handleClickAddManager = () => {

        let formAdd = this.state.formAdd
            formAdd.visible = true
            //clear data form
            formAdd.field.member_id = null
            formAdd.field.member_pass =  null
            formAdd.field.member_credit = 0
            formAdd.field.member_name =  null
            formAdd.field.member_status =  'open'
            formAdd.field.member_priority =  'senior'
            formAdd.field.member_choice_pay =  'Daily'
            formAdd.field.member_promise_pay =  []
            formAdd.field.percent =  0
        this.setState({formAdd : formAdd})

    }

    handleClickEditManager = () => {

        let formEdit = this.state.formEdit
            formEdit.visible = true
            //clear data form
            formEdit.field.member_id = 'BeamSuper'
            formEdit.field.member_pass =  '1234'
            formEdit.field.member_credit = 50000
            formEdit.field.member_name =  'Beam'
            formEdit.field.member_status =  'open'
            formEdit.field.member_choice_pay =  'Daily'
            formEdit.field.member_promise_pay =  ['Mon']
            formEdit.field.percent = 40
        this.setState({formEdit : formEdit})

    }

    formAddManagerCallback = (_state,_value) => {

        if(_state === 'Cancel'){
            let formAdd = this.state.formAdd
                formAdd.visible = false
            this.setState({formAdd : formAdd})
        }else{
            console.log({_value})
        }

    }

    formEditManagerCallback = (_state,_value) => {

        if(_state === 'Cancel'){
            let formEdit = this.state.formEdit
                formEdit.visible = false
            this.setState({formEdit : formEdit})
        }else{
            console.log({_value})
        }

    }


    render(){
         
        const { fetch , list , columns } = this.state.table
        const { formAdd , formEdit } = this.state
        const { special_priority } = this.props
        console.log({formEdit})
        return (
            <div>
                <Row>
                    <Col xs={24}>
                        <PathTitle textPath="ข้อมูลสมาชิก - รายชื่อซีเนียร์" />
                    </Col>
                    <Col xs={24}>
                        <button className="dg-button-success pull-right" onClick={()=>{this.handleClickAddManager()}} >
                            ซีเนียร์ใหม่
                        </button>
                        <button className="dg-button-success pull-right" onClick={()=>{this.handleClickEditManager()}} >
                            แก้ไขซีเนียร์
                        </button>
                    </Col>
                    <Col xs={24}>
                        <Table size="small" style={{marginTop:15}} columns={columns} dataSource={list} bordered />
                    </Col>
                </Row>
                <Row>
                    <FormManager dataDefault={formEdit.field}  callback={this.formEditManagerCallback} title="แก้ไข ซีเนียร์" loading={formEdit.loading} show={formEdit.visible} percent={100}  credit={500000} />
                    {/* <FormMember dataDefault="d"  callback={this.formEditManagerCallback} title="เพิ่ม ซีเนียร์" loading={loading} show={visible}  credit={200000} /> */}
                </Row>
            </div>
        )
    }

}


const mapStateToProps = state => {
    return { auth: state.auth }
}

export default connect( mapStateToProps )( Senior )