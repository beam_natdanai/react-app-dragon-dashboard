import React, { Component } from "react"
import { connect } from 'react-redux'
import { Row, Col } from 'antd'
import { PathTitle } from '../../components/pathTitle'
import { BtnAddMember } from '../../components/btnAddMember'
import { ModalAddMember } from '../../components/modalAddMember'
import { ModalEditMember } from '../../components/modalEditMember'
import { ModalConfirm } from '../../components/modalConfirm'
import { TableList } from '../../components/tableList'
import { Service as ServiceMember } from '../../services/member'
import { Radio } from 'antd'
import { Checkbox } from 'antd'
import { Service } from '../../services/member' 
import { Service as ServiceCredit } from '../../services/credit'
import { numberWithCommas , formatDate , check_is_english  } from '../../lib/utils' 


const RadioGroup = Radio.Group

class Watcher extends Component {

    constructor(props) {
        super(props)
        this.state = {
            adminId:'DG',
            member:{credit:0,result_credit:0},
            result_percent:0,
            visibleForm: false,
            visibleFormEdit: false,
            dataLists:[],
            formAllState:{
                Add:{key:'Add',title:'เพิ่ม ผู้ช่วย'},
                Edit:{key:'Edit',title:'แก้ไข ผู้ช่วย'}
            },
            dataEcditPass:undefined,
            inputEditPass:null,
            visibleEditpass: false,
            formState:undefined,
            payChoiceAllState:{
                Power:'Power',
                Daily:'Daily',
                Weekly:'Weekly'
            },
            form:{
                member_id:undefined,
                member_pass:undefined,
                member_name:undefined,
                member_priority:'watcher',
                member_status:'open',
            },
            formEdit:{
                member_id:undefined,
                member_pass:undefined,
                member_name:undefined,
                member_priority:'watcher',
                member_status:'open',
            },
            columns:[
                {
                  title: "ลำดับ",
                  dataIndex: "index",
                  align:'center',
                  render : (text, record, index) => (index + 1)
                },
                {
                  title: "รหัสผู้ใช้",
                  dataIndex: "id",
                  align:'center'
                },
                {
                  title: "ติดต่อ",
                  dataIndex: "name",
                  align:'center'
                },
                {
                  title: "ตั้งค่า",
                  dataIndex: "setting",
                  render: (text, record, index) => <a disabled={(this.isWatcher(this.props.special_priority.priority))} onClick={()=>{this.handleClickEdit(record)}} > แก้ไข </a>,
                  align:'center'
                },
                {
                  title: "วันที่เพิ่ม",
                  dataIndex: "create_date",
                  align:'center'
                },
                {
                  title: "สถาณะ",
                  dataIndex: "status",
                  render: (text, record, index) => {
                        return(
                            <div>{(record.status === 'open' ? 
                                <p style={{color:'#31B42F'}}>{record.status}</p>
                            : 
                                <p style={{color:'#F05B3B'}} >{record.status}</p>
                            )}</div>
                        )
                  },
                  align:'center'
                },
                {
                    title: "แก้ไขรหัสผ่าน",
                    dataIndex: "edit_pass",
                    render: (text, record, index) => {
                        return(
                          <button disabled={(this.isWatcher(this.props.special_priority.priority))} key="btnEditPass" style={{color:'#ffc235',fontSize:13,borderRadius:3,backgroundColor:'#f4f4f4',cursor:'pointer',minWidth:60}} onClick={()=>{this.handleClickEditPass(record)}} >
                                แก้ไข
                          </button>
                        )
                    },
                    align:'center'
                }
            ],
            member_receive:undefined,
            creditTopup:0,
            parent:{
                token:undefined,
                id:undefined
            }
        }
    }

    componentDidMount() {

        if(this.props.special_priority.priority === 'watcher'){
            if(this.state.parent.token === undefined ){
                this.getPriolityById(this.props.auth.token,this.props.special_priority.parentId).then((res)=>{
                    if(res.status){
                        if(res.status){
                            this.setState({
                                parent:{token:res.data.tokens[0].token,id:this.props.auth.member._id}
                            })
                            this.init_watcher(res.data.tokens[0].token,this.props.auth.member._id,this.state.form.member_priority)
                        }
                    }
                })
            }
        }else{
            if(this.props.auth.token !== null){
                this.init_watcher(this.props.auth.token,this.props.auth.member._id,this.state.form.member_priority)
            }
        }
    }

    componentDidUpdate(preProps){
        if(this.props.special_priority.priority === 'watcher'){
            if(this.state.parent.token === undefined ){
                this.getPriolityById(this.props.auth.token,this.props.special_priority.parentId).then((res)=>{
                    if(res.status){
                        if(res.status){
                            this.setState({
                                parent:{token:res.data.tokens[0].token,id:this.props.auth.member._id}
                            })
                            this.init_watcher(res.data.tokens[0].token,this.props.auth.member._id,this.state.form.member_priority)
                        }
                    }
                })
            }
        }else{
            if(preProps.auth.token !== this.props.auth.token){
                if(this.props.auth.token !== null){
                    this.init_watcher(this.props.auth.token,this.props.auth.member._id,this.state.form.member_priority)
                }
            }
        }
    }

    getPriolityById = (token,_id) => {
        return new Promise((resolve, reject) => {
          ServiceMember.getById(token,_id).then((res)=>{
              if(res.status){
                resolve({status:true,data : res.data.data})
              }else{
                resolve({status:false})
              }
          })
        }); 
    }

    init_credit = () => {
        Service.getCredit(this.props.auth.token).then((res,err)=>{
            if(res.status){
                let obj = this.state.member
                    obj.credit = res.data.credit
                    obj.result_credit = res.data.credit
                this.setState({member:obj})
            }else{
                console.log("get error")
            }
        })
    }

    handleClickAdd = () => {
        this.setState({visibleForm:true,formState:this.state.formAllState.Add})
    }

    handleClickEdit = (_record) => {
        let formObj = {
            id:_record._id,
            member_id:_record.id,
            member_credit:_record.balanceInt,
            member_name:_record.name,
            member_status:_record.status,
            member_choice_pay : _record.member_choice_pay,
            member_promise_pay : _record.member_promise_pay,
            percent : _record.percent
        }
        this.setState({visibleFormEdit:true,formState:this.state.formAllState.Edit,formEdit:formObj})
    }

    init_watcher = (_token,_id,_priority) => {
        Service.getByIdParents(_token,_id,_priority).then( async (res,err)=>{
            if(res.status){
                this.formatData(res.data).then((resFormat,errFormat) => {
                    this.setState({dataLists:resFormat.data})
                })
            }else{
                console.log("get member error !!")
            }
        })
    }

    clearForm = () => {

        let formInit = {
            member_id:undefined,
            member_pass:undefined,
            member_name:undefined,
            member_priority:'watcher',
            member_status:'open',
        }
        window.document.getElementById('form').reset()
        this.setState({form:formInit,visibleForm:false,result_percent:this.props.auth.member.percent})

    }

    formatData = (_array) => {
        let results = []
        return new Promise((resolve, reject) => {
            _array.forEach((element,index) => {
                let dataObj = {
                    _id : element.member._id,
                    id : element.member.member_id,
                    name : element.member.member_name,
                    credit_limit : numberWithCommas(element.member.member_credit),
                    create_date : formatDate(element.member.createdAt),
                    percent : element.percent,
                    member_choice_pay : element.member.member_choice_pay,
                    member_promise_pay : element.member.member_promise_pay,
                    status : element.member.member_status
                }
                results.push(dataObj)
                if(index === _array.length - 1){
                    resolve({status:true,data:results})
                }
                
            });
        });

    }

    clearFormEdit = () => {
        window.document.getElementById('formEdit').reset()
        this.setState({visibleFormEdit:false})
    }

    modalEditCallback = (_status) => {

        if(_status === 'Cancel'){
            
            this.clearFormEdit()

        }else{  

            if(this.state.formEdit.member_id === undefined || this.state.formEdit.member_id === ""){
                alert('กรุณากรอกรหัสด้วยค่ะ')
            }
            if(this.state.formEdit.member_name === undefined || this.state.formEdit.member_name === ""){
                alert('กรุณากรอกชื่อด้วยค่ะ')
            }
            if(this.state.formEdit.member_status === undefined || this.state.formEdit.member_status === ""){
                alert('กรุณากรอกชื่อด้วยค่ะ')
            }
            else{

                let dataObj = {
                    member_id:this.state.formEdit.member_id,
                    member_name:this.state.formEdit.member_name,
                    member_status:this.state.formEdit.member_status,
                    member_choice_pay:this.state.formEdit.member_choice_pay,
                    member_promise_pay:this.state.formEdit.member_promise_pay,
                }

                Service.update(this.props.auth.token,this.state.formEdit.id,dataObj).then((res,err) => {

                    if (res.status) {
                            alert("แก้ไขข้อมูลสำเร็จค่ะ")
                            this.clearFormEdit()
                            this.init_watcher(this.props.auth.token,this.props.auth.member._id,this.state.form.member_priority)
                    }else{
                        alert("แก้ไขข้อมูลไม่สำเร็จค่ะ กรุณาลองใหม่")
                        this.clearFormEdit()
                        this.setState({visibleFormEdit:false})
                    }
    
                })
            }
        }

    }

    modalAddCallback = (_status) => {

        if(_status === 'Cancel'){
            
            this.clearForm()

        }else{

            if(this.state.form.member_id === undefined || this.state.form.member_id === ''){
                alert('กรุณากรอกรหัสด้วยค่ะ')
            }
            else if(this.state.form.member_pass === undefined || this.state.form.member_pass === ''){
                alert('กรุณากรอกรหัสผ่านด้วยค่ะ')
            }
            else if(this.state.form.member_name === undefined || this.state.form.member_name === ''){
                alert('กรุณากรอกชื่อด้วยค่ะ')
            }
            else{
                let obj = this.state.form
                    // obj.member_id = this.props.auth.member.member_id + obj.member_id
                    obj.member_id =  this.state.adminId  +  obj.member_id
                Service.add(this.props.auth.token,this.state.form).then((res,err) => {
                    if (res.status) {
                            alert("เพิ่มข้อมูลสำเร็จค่ะ")
                            this.clearForm()
                            this.init_watcher(this.props.auth.token,this.props.auth.member._id,this.state.form.member_priority)
                            this.init_credit()
                        }else{
                        alert("เพิ่มข้อมูลไม่สำเร็จค่ะ รหัสผู้ใช้ นี้อาจมีผู้ใช้อยู่แล้วค่ะ กรุณาลองใหม่ค่ะ ")
                        this.clearForm()
                    }
                })

            }

        }

    }

    clearTopupCredit = () => {
        window.document.getElementById('formTopupCredit').reset()
        this.setState({creditTopup:0,visibleCredit:false})
    }

    clearEditPassCallback = () => {
        document.getElementById('inputEditPass').value = ""
        this.setState({visibleEditpass : false,inputEditPass : null})
    }

    modalConfirmEditPassCallback = (_status) => {
        if(_status === 'Cancel'){ 
            this.clearEditPassCallback()
        }
        else{
            if(this.state.inputEditPass === "" || this.state.inputEditPass === null){
                alert("กรุณากรอกรหัสผ่านด้วยค่ะ")
            }else{
                Service.update(this.props.auth.token,this.state.dataEcditPass._id,{member_pass : this.state.inputEditPass}).then((res,err) => {
                    if(res.status){
                        alert("แก้ไขรหัสผ่าน สำเร็จค่ะ")
                        this.clearEditPassCallback()
                    }else{
                        alert("แก้ไขไม่สำเร็จค่ะ กรุณาลองอีกครั้ง")
                    }
                })
            }
        }
    }

    onStatusChange = e => {
        let stateObj = this.state.form
        stateObj.member_status = e.target.value
        this.setState({
            form: stateObj,
        });
    }

    onStatusEditChange = e => {
        let stateObj = this.state.formEdit
        stateObj.member_status = e.target.value
        this.setState({
            formEdit: stateObj,
        });
    }


    handleInputEditChange = ( event , _name ) => {
        let stateObj = this.state.formEdit
        stateObj[_name] = event.target.value
        this.setState({
            formEdit: stateObj,
        });
    }

    onPayChoiceChange = e => {
        let stateObj = this.state.form
        stateObj.member_choice_pay = e.target.value
        this.setState({
            form: stateObj,
        });
    }

    onPayChoiceEditChange = e => {
        let stateObj = this.state.formEdit
        stateObj.member_choice_pay = e.target.value
        this.setState({
            formEdit: stateObj,
        });
    }

    onDayCheckBoxChange = (checkedValues) => {
        let stateObj = this.state.form
        stateObj.member_promise_pay = checkedValues
        this.setState({
            form: stateObj,
        });
    }

    onDayCheckBoxEditChange = (checkedValues) => {
        let stateObj = this.state.formEdit
        stateObj.member_promise_pay = checkedValues
        this.setState({
            formEdit: stateObj,
        });
    }

    handleInputChange = ( event , _name ) => {
        
        let stateObj = this.state.form
        stateObj[_name] = event.target.value
        if(_name === 'percent'){
            // percent change
                if(!isNaN(parseInt(event.target.value)) && parseInt(event.target.value) >= 0 ){
                    if(parseInt(event.target.value) <= this.props.auth.member.percent){
                        if(this.props.auth.member.percent >= parseInt(event.target.value)){
                            document.getElementById('input_percent').value = parseInt(event.target.value)
                            stateObj[_name] = parseInt(event.target.value)
                            this.setState({
                                form: stateObj,
                                result_percent:( this.props.auth.member.percent - parseInt(event.target.value) )
                            });
                        }
                    }else{
                        document.getElementById('input_percent').value = ""
                        this.setState({
                            form: stateObj,
                            result_percent: ( this.props.auth.member.percent )
                        });
                    }
                }else{
                    document.getElementById('input_percent').value = ""
                    this.setState({
                        form: stateObj,
                        result_percent: this.props.auth.member.percent
                    });
                }
        }
        else if(_name === 'member_id'){
            if(check_is_english(event.target.value)){
                this.setState({
                    form: stateObj,
                });
            }else{
                document.getElementById('member_id').value = ""
                stateObj[_name] = ""
                this.setState({
                    form: stateObj,
                });
            }
        }

        else if(_name === 'member_pass'){
            if(check_is_english(event.target.value)){
                this.setState({
                    form: stateObj,
                });
            }else{
                document.getElementById('member_pass').value = ""
                stateObj[_name] = ""
                this.setState({
                    form: stateObj,
                });
            }
        }

        else if(_name === 'member_credit'){
            // credit change
            let num = parseFloat((event.target.value).replace(/,/g, ''))
            let numStr = numberWithCommas(num)
            let member = this.state.member
            if(!isNaN(parseInt(event.target.value)) && num >= 0 ){
                if(num <= this.state.member.credit){
                    if(this.state.member.credit >= num){
                        document.getElementById('input_credit').value = numStr
                        stateObj[_name] = num
                        member.result_credit = ( this.state.member.credit - num )
                        this.setState({
                            form : stateObj,
                            member : member
                        });
                    }
                }else{
                    document.getElementById('input_credit').value = ""
                    member.result_credit = this.state.member.credit
                    this.setState({
                        form: stateObj,
                        member: member
                    });
                }
            }else{
                document.getElementById('input_credit').value = ""
                member.result_credit = this.state.member.credit
                this.setState({
                    form : stateObj,
                    member : member
                });
            }
        }
        else{  

            let stateObj = this.state.form
            stateObj[_name] = event.target.value
            this.setState({
                form: stateObj,
            });
                
        }   
        
    }

    handleClickTopupCredit = (_record) =>{
        this.setState({visibleCredit:true,member_receive:_record._id})
    }

    handleChangeTopup = (event) => {

        let member = this.state.member
        if(parseInt(event.target.value) !== NaN && parseInt(event.target.value) >= 0 ){
            if(parseInt(event.target.value) <= this.state.member.credit){
                if(this.state.member.credit >= parseInt(event.target.value)){
                    document.getElementById('inputTopupCredit').value = parseInt(event.target.value)
                    member.result_credit = ( this.state.member.credit - parseInt(event.target.value) )
                    this.setState({
                        member : member
                    });
                }
            }else{
                document.getElementById('inputTopupCredit').value = ""
                member.result_credit = this.state.member.credit
                this.setState({
                    member: member
                });
            }
        }else{
            document.getElementById('inputTopupCredit').value = ""
            member.result_credit = this.state.member.credit
            this.setState({
                member : member
            });
        }

    }

    handleChangePass = (event) => {

        if(check_is_english(event.target.value)){
            this.setState({
                inputEditPass : event.target.value,
            });
        }else{
            document.getElementById('inputEditPass').value = ""
            this.setState({
                inputEditPass : event.target.value,
            });
        }

    }

    handleClickEditPass = (_record) => {
        this.setState({visibleEditpass : true , dataEcditPass : _record})
    }

    isWatcher = (_data) => {
        if(_data === 'watcher'){
            return true
        }else{
            return false
        }
    }

    render(){

        const { visibleForm , visibleFormEdit , formState , form , formEdit , payChoiceAllState , dataLists , columns , visibleCredit , result_percent , visibleEditpass , member } = this.state
        const { special_priority } = this.props

        return (
            <div>
                <Row>
                    <Col span={24}>
                        <PathTitle textPath="ข้อมูลสมาชิก - รายชื่อผู้ช่วย" />
                    </Col>
                    <Col span={24}>
                        <BtnAddMember status={(this.isWatcher(special_priority.priority))} textBtn="ผู้ช่วยใหม่" onClick={()=>{this.handleClickAdd()}} />
                        <ModalAddMember title={(formState === undefined ? null : formState.title )} callback={this.modalAddCallback} show={visibleForm} >
                            <form id="form">

                                <Row>
                                    <Col xs={24} md={12} >
                                        <Row gutter={16} >
                                            <Col xs={8} md={8}>
                                                <p style={{float:'right'}}>รหัสผู้ช่วย :</p>
                                            </Col>
                                            <Col xs={16} md={16}>
                                                {this.props.auth.token !== null ? <p style={{float:'left'}} > {` DG `} &nbsp;</p> : null } <input id="member_id" maxLength={16} style={{float:'left',minWidth:80,maxWidth:90,border:'solid',borderWidth:1}} defaultValue={form.member_id} onChange={(e)=>{this.handleInputChange(e,'member_id')}}  />
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={24} md={12} > 
                                        <Row gutter={16}>
                                            <Col xs={10} md={8}>
                                                <p style={{float:'right'}}>รหัสผ่าน :</p>
                                            </Col>
                                            <Col xs={14} md={16}>
                                                <input maxLength={16} id="member_pass" defaultValue={form.member_pass} style={{float:'left',minWidth:60,maxWidth:120,border:'solid',borderWidth:1}} onChange={(e)=>{this.handleInputChange(e,'member_pass')}}  />
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col xs={24} md={12} >
                                        <Row gutter={16}>
                                            <Col xs={10} md={8}>
                                                <p style={{float:'right'}}>ติดต่อ :</p>
                                            </Col>
                                            <Col xs={14} md={16}>
                                                <input maxLength={15} style={{float:'left',minWidth:60,maxWidth:120,border:'solid',borderWidth:1}} onChange={(e)=>{this.handleInputChange(e,'member_name')}}  />
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={24} md={12} > 
                                        <Row gutter={16} >
                                            <Col xs={10} md={8}>
                                                <p style={{float:'right'}}>สถาณะ :</p>
                                            </Col>
                                            <Col xs={14} md={16}>
                                                <RadioGroup onChange={this.onStatusChange} value={form.member_status}>
                                                    <Radio value={'open'}>เปิด</Radio>
                                                    <Radio value={'close'}>ปิด</Radio>
                                                </RadioGroup>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </form>
                        </ModalAddMember>
                        <ModalEditMember title={(formState === undefined ? null : formState.title )} callback={this.modalEditCallback} show={visibleFormEdit} >
                            <form id="formEdit">
                                <Row>
                                    <Col xs={24} md={12} >
                                        <Row gutter={16} >
                                            <Col xs={8} md={8}>
                                                <p style={{float:'right'}}>รหัสผู้ช่วย :</p>
                                            </Col>
                                            <Col xs={16} md={16}>
                                                {this.props.auth.token !== null ? <p style={{float:'left'}} > {` ${this.props.auth.member.member_id} `} &nbsp;</p> : null } <input maxLength={16} style={{float:'left',minWidth:80,maxWidth:90,border:'solid',borderWidth:1}} defaultValue={form.member_id} defaultValue={formEdit.member_id} onChange={(e)=>{this.handleInputEditChange(e,'member_id')}}  />
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={24} md={12} > 
                                        <Row gutter={16}>
                                            <Col xs={10} md={8}>
                                                <p style={{float:'right'}}>รหัสผ่าน :</p>
                                            </Col>
                                            <Col xs={14} md={16}>
                                                <input maxLength={16} disabled  style={{float:'left',minWidth:60,maxWidth:120,border:'solid',borderWidth:1}} defaultValue={form.member_id} onChange={(e)=>{this.handleInputEditChange(e,'member_pass')}}  />
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col xs={24} md={12} >
                                        <Row gutter={16}>
                                            <Col xs={10} md={8}>
                                                <p style={{float:'right'}}>ติดต่อ :</p>
                                            </Col>
                                            <Col xs={14} md={16}>
                                                <input maxLength={15} style={{float:'left',minWidth:60,maxWidth:120,border:'solid',borderWidth:1}} defaultValue={form.member_id} defaultValue={formEdit.member_name} onChange={(e)=>{this.handleInputEditChange(e,'member_name')}}  />
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={24} md={12} > 
                                        <Row gutter={16} >
                                            <Col xs={10} md={8}>
                                                <p style={{float:'right'}}>สถาณะ :</p>
                                            </Col>
                                            <Col xs={14} md={16}>
                                                <RadioGroup onChange={this.onStatusEditChange} value={formEdit.member_status}>
                                                    <Radio value={'open'}>เปิด</Radio>
                                                    <Radio value={'close'}>ปิด</Radio>
                                                </RadioGroup>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </form>
                        </ModalEditMember>           
                        <ModalConfirm title="แก้ไขรหัสผ่าน" buttonOk="บันทึก" buttonCancel="ยกเลิก" callback={this.modalConfirmEditPassCallback} show={visibleEditpass} >
                            <form id="formEditPass">
                                <Row>
                                    <Col style={{textAlign:'center'}} span={24}>
                                        รหัสผ่าน : <input maxLength={16} onChange={this.handleChangePass} id="inputEditPass" style={{width:90,border:'solid',borderWidth:1}} /> 
                                    </Col>
                                </Row>
                            </form>
                        </ModalConfirm>
                    </Col>
                </Row>
                <div style={{height:10}}></div>
                <Row>
                    <Col span={24}>
                        <TableList columns={columns} dataList={dataLists} />
                    </Col>
                </Row>
            </div>
        )
    }

}

const mapStateToProps = state => {
    return { auth: state.auth }
}

export default connect( mapStateToProps )(Watcher)