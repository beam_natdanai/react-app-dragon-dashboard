import React, { Component } from "react"
import { Service } from '../../services/auth' 
import { Form, Icon, Input, Button, Checkbox } from 'antd'
import { connect } from 'react-redux'
import { Card } from 'antd'
import { Spin, Alert } from 'antd'
import { isEmpty } from '../../lib/utils'
import authAction from '../../redux/auth/authRedux'
import Session, { setItem , getItem , clear } from '../../lib/session'


class Signin extends Component {

  constructor( pros ) {
    super( pros )
    this.state = {
      loading: false,
    }
  }

  componentDidMount(){
    if ( !isEmpty(getItem( Session.header )) ) {
      this.props.history.push( '/' )
    }
  }

  componentDidUpdate( preProps ) {
    if ( preProps.auth.token !== this.props.auth.token ) {
      if ( this.props.auth.token !== null ) {
          this.props.history.push( '/' )
      }
    }
  }

  handleSubmit = ( e ) => {

    e.preventDefault();
    this.props.form.validateFields(( err, values ) => {
      if ( !err ) {

        this.isLoading(true)

        Service.login((values.username).replace(/\s/g, '') , (values.password).replace(/\s/g, '')).then((res,err) => {
          
          if(res.status){
            alert("ยินดีต้อนรับเข้าสู่ระบบค่ะ")
                setItem( Session.header,{
                  member:{...res.data.data.member,...res.data.data.memberTransaction},
                  token:res.data.data.token
                })
                this.props.loginSuccess(res.data.data)
          }else{
            alert("หรัสผ่าน หรือ รหัสผู้ใช้ผิด")
            this.isLoading(false)
          }
          
        })

      }
    } );
  }

  isLoading = (_bool) => {
    this.setState( { loading: _bool } )
  }

  pageUnLoading = () => {
    this.setState( { loading: false } )
  }

  render(){

    const { getFieldDecorator } = this.props.form;

    return ( 
      <Spin spinning={this.state.loading} delay={300} tip="Loading...">
        {this.props.auth.loginError && <Alert message="ลงชื่อเข้าใช้ไม่สำเร็จ" description="กรุณาตรวจสอบความถูกต้องของชื่อผู้ใช้และรหัสผ่านให้ตรงกัน" type="error" showIcon="showIcon"/>}
        <div style={{
            marginTop: 60
          }}>
          <center>
            <Card style={{
                width: 500
              }}>
              <Form onSubmit={this.handleSubmit} className="login-form">
                <Form.Item>
                  <div style={{
                      textAlign: "center"
                    }}>
                    
                    <h3>Dragon Dashboard</h3>
                  </div>
                </Form.Item>
                <Form.Item>
                  {
                    getFieldDecorator( 'username', {
                      rules: [
                        {
                          required: true,
                          message: 'กรุณาใส่ชื่อผู้ใช้'
                        }
                      ]
                    } )( <Input  maxLength={16} prefix={<Icon type = "user" style = {{ color: 'rgba(0,0,0,.25)' }}/>} placeholder="ชื่อผู้ใช้ / Username"/> )
                  }
                </Form.Item>
                <Form.Item>
                  {
                    getFieldDecorator( 'password', {
                      rules: [
                        {
                          required: true,
                          message: 'กรุณาใส่รหัสผ่าน!'
                        }
                      ]
                    } )( <Input  maxLength={16} prefix={<Icon type = "lock" style = {{ color: 'rgba(0,0,0,.25)' }}/>} type="password" placeholder="รหัสผ่าน / Password"/> )
                  }
                </Form.Item>
                <Form.Item>
                  <Button block={true} type="primary" htmlType={'submit'} className={'login-form-button'}>
                    เข้าสู่ระบบ
                  </Button>
                </Form.Item>
              </Form>
            </Card>
          </center>
        </div>
      </Spin> );
  }

}

const WrappedNormalLoginForm = Form.create( { name: 'normal_login' } )( Signin );

const mapStateToProps = state => {
  return { auth: state.auth }
}

const mapDispatchToProps = dispatch => {
  return {
    loginSuccess: ( _dataObj ) => {
      dispatch( authAction.loginSuccess( _dataObj ) )
    }
  }
}

export default connect( mapStateToProps, mapDispatchToProps )( WrappedNormalLoginForm )
