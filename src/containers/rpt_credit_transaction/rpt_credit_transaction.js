import React, { Component } from 'react'
import { Row, Col } from 'antd'
import { connect } from 'react-redux'
import { PathTitle } from '../../components/pathTitle'
import { TableReport } from '../../components/tableReport'
import { Service } from '../../services/member'
import { Service as ServiceCreditTrans } from '../../services/report'
import { numberWithCommas , formatDate } from '../../lib/utils' 
import moment from 'moment'
import { DatePicker } from 'antd'
import { Select } from 'antd'
import 'moment/locale/th';

const { Option } = Select

class Rpt_credit_transaction extends Component {

    constructor(props) {
        super(props)
        this.state = {
            members:[],
            filter:{
                search:null,
                dateStart  :moment((new Date())).format('YYYY-MM-DD'),
                dateFinish : moment((new Date())).format('YYYY-MM-DD'),
            },
            member:null,
            dataList:[],
            column:[
                {
                    title: "ลำดับ",
                    dataIndex: "index",
                    align:'center',
                    render : (text, record, index) => (index + 1)
                },
                {
                    title: "รหัส",
                    dataIndex: "name",
                    align:'center',
                    render : (text, record, index) => {
                        if(this.state.member !== null){
                            return this.state.member.member_id
                        }
                    }
                },
                {
                    title: "วันที่",
                    dataIndex: "createdAt",
                    align:'center',
                    render : (text, record, index) => (formatDate(text))
                },
                {
                    title: "จำนวนเงิน",
                    dataIndex: "credit_transfer",
                    align:'center',
                    render : (text, record, index) => {

                        if(record.credit_transfer <= 0){
                            return <p style={{color:'#FF5733'}} >{numberWithCommas(record.credit_transfer)}</p>
                        }
                        else if(record.credit_transfer === 0){
                            return <p style={{color:'#000000'}} >{numberWithCommas(record.credit_transfer)}</p>
                        }
                        else{
                            return <p style={{color:'#1FDE7A'}} >{numberWithCommas(record.credit_transfer)}</p>
                        }
                        
                    }
                }
            ]
        }
    }

    componentDidMount() {

        if(this.props.auth.token !== null){
            this.init_employees(this.props.auth.token,this.props.auth.member._id)
            
        }

    }

    componentDidUpdate(preProps){
        if(preProps.auth.token !== this.props.auth.token){
            if(this.props.auth.token !== null){
                this.init_employees(this.props.auth.token,this.props.auth.member._id)
            }
        }
    }

    onChange = (value) => {
        let obj = this.state.filter
        obj.search = value
        this.setState({filter:obj})
    }

    handleClickSearch = () => {
        if(this.state.filter.search !== null){

            let obj = {
                type:null,
                from:this.state.filter.dateStart,
                to:this.state.filter.dateFinish
            }

            if(this.state.filter.dateStart === null && this.state.filter.dateFinish === null){
                obj.type = 'all'
            }
            else if(this.state.filter.dateStart !== null && this.state.filter.dateFinish === null){
                obj.type = 'day'
            }
            else if(this.state.filter.dateStart !== null && this.state.filter.dateFinish !== null){
                obj.type = 'between'
            }
            

            this.init_creditTransaction(this.props.auth.token,this.state.filter.search,obj)
        }
    }

    init_creditTransaction = (_token,_id,_dataObj) => {
        ServiceCreditTrans.getReportCreditTransaction(_token,_id,_dataObj).then( async (res,err)=>{

            if(res.status){

                if(res.data.data.length > 0){
                    const obj = {
                        member_del: res.data.data[0].member_del,
                        member_id: res.data.data[0].member_id,
                        member_name: res.data.data[0].member_name,
                        member_priority: res.data.data[0].member_priority
                    }
                    this.setState({dataList: res.data.data[0].creditHistory,member: obj})
                }
            }else{
                console.log("get credit error !!")
            }
        })
    }

    init_employees = (_token,_id) => {
        Service.getByIdParentsForReport(_token,_id).then( async (res,err)=>{

            if(res.status){
                this.setState({members:res.data})
            }else{
                console.log("get member error !!")
            }
        })
    }

    onDatePickerChange = ( date, dateString, dateType ) => {
        const newDate =  new Date(dateString)
        let obj = this.state.filter
        if(dateType === 'start'){
            obj.dateStart = dateString
            this.setState({
                filter:obj,
            })
        }else{

            const dateStart = new Date(this.state.filter.dateStart)
            if(newDate.getTime(dateString) < dateStart){
                alert("วันที่สิ้นสุดต้องเป็นวันที่ หลังจากวันที่เริ่มต้น")
            }else{
                obj.dateFinish = dateString
                this.setState({
                    filter:obj
                })
            }

        }

    }


    render(){

        const { members , column , dataList } = this.state
        const { dateStart , dateFinish } = this.state.filter

        return (    
            <div>
                <Row>
                    <Col span={24}>
                        <PathTitle textPath="รายงาน - ประวัติเครดิต (รายละเอียด)" />
                    </Col>
                </Row>
                <div style={{height:10}}></div>
                <Row>
                    <Col xs={24} md={7}>
                        <Select
                            showSearch
                            style={{ width: 300 }}
                            placeholder="กรุณาเลือก สมาชิกของท่าน"
                            optionFilterProp="children"
                            onChange={this.onChange}
                            filterOption={(input, option) =>
                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            {members.map((row,index)=>{
                                if(row.member.member_priority === 'watcher'){
                                    return null
                                }else{
                                    return <Option key={`option_${index}`} value={row.member._id}>{`[ ${row.member.member_priority} ] ${row.member.member_id} ( ${row.member.member_name} )`}</Option>
                                }
                            })}
                        </Select>
                    </Col>
                    <Col xs={24} md={8}>
                        <Row gutter={16}>
                            <Col xs={12}>
                                <DatePicker value={moment(dateStart,'YYYY-MM-DD')} style={{float:'right'}} placeholder="เลือกวันเริ่มต้น" onChange={( date , dateString )=>{this.onDatePickerChange(date,dateString,'start')}} />   
                            </Col>
                            <Col xs={12}>
                                <DatePicker value={moment(dateFinish,'YYYY-MM-DD')} placeholder="เลือกวันสิ้นสุด" onChange={( date , dateString )=>{this.onDatePickerChange(date,dateString,'end')}} />   
                            </Col>
                        </Row>
                    </Col>
                    <Col xs={24} md={9}>
                        <button key="search" style={{float:'right',fontSize:15,borderRadius:4,height:32,backgroundColor:'#f4f4f4',cursor:'pointer',minWidth:100,borderColor:'rgb(249, 139, 139)',color:'rgb(249, 139, 139)'}} onClick={()=>{this.handleClickSearch()}} >
                                ค้นหา
                        </button>
                    </Col>
                </Row>
                <Row>
                    <br/>
                    <Col xs={24} >
                        <TableReport dataList={dataList} columns={column} />
                    </Col>
                </Row>
            </div>
        )
    }

}

const mapStateToProps = state => {
    return { auth: state.auth }
}

export default connect( mapStateToProps )(Rpt_credit_transaction)
