import React, { Component } from "react"
import { connect } from 'react-redux'
import { Row, Col } from 'antd'
import { PathTitle } from '../../components/pathTitle'
import { Service } from '../../services/member' 


class ManageLoseWin extends Component {

    constructor(props) {
        super(props)
        this.state = {
            percent:0
        }
    }

    componentDidMount() {
        if(this.props.auth.token !== null){
            this.init_chanceOfWinning()
        }
    }

    componentDidUpdate(preProps){
        if(preProps.auth.token !== this.props.auth.token){
            if(this.props.auth.token !== null){
                this.init_chanceOfWinning()
            }
        }
    }

    init_chanceOfWinning = () => {
        Service.getChanceOfWinning(this.props.auth.token).then((res,err)=>{
            if(res.status){
                this.setState({percent:res.data.percent})
            }else{
                console.log("get error")
            }
        })
    }

    hadleChangeInput = (_event) => {
        let value = _event.target.value
        if(parseInt(value) !== NaN && (parseInt(value) > 0 &&  parseInt(value) <= 100 ) ){

            document.getElementById('percentInput').value = parseInt(value)
            this.setState({percent:parseInt(value)})

        }else{
            document.getElementById('percentInput').value = ""
            this.setState({percent:0})
        }
    }

    handleClickSave = () => {
        // console.log("percent => ",this.state.percent)
        if(parseInt(this.state.percent) < 0 ){
            alert("Percent น้อยกว่า 0 ")
        }else{
            Service.updateChanceOfWinning(this.props.auth.token,this.props.auth.member._id,this.state.percent).then((res,err)=>{
                if(res.status){
                    alert("อัพเดท โอกาสชนะเรียบร้อยค่ะ")
                    this.init_chanceOfWinning()
                }else{
                    console.log("get error")
                }
            })
        }

    }

    handleClickCancel = () => {
        this.init_chanceOfWinning()
    }

    render(){

        const { percent } = this.state
        const { special_priority } = this.props

        return (
            <div>
                {(special_priority.priority === 'watcher' ?
                    <Row>
                        <Col span={24}>
                            <PathTitle textPath="จัดการ - โอกาส แพ้ ชนะ" />
                        </Col>
                        <Col span={24}>
                            <div style={{textAlign:'center'}}>
                                <h3>ไม่มีสิทธิ์เข้าถึง</h3>
                            </div>
                        </Col>
                    </Row>
                 : 
                    <Row>
                        <Col span={24}>
                            <PathTitle textPath="จัดการ - โอกาส แพ้ ชนะ" />
                        </Col>
                        <Col span={24}>
                            <div style={{textAlign:'center'}}>
                                โอกาสชนะ (ผู้เล่น) : &nbsp;<input id="percentInput" value={percent} style={{width:40,border:'solid',borderWidth:1,textAlign:'center'}} onChange={this.hadleChangeInput} maxLength={3}  /> &nbsp;%
                            </div>
                            <div style={{textAlign:'center'}}>
                                โอกาสชนะ (บริษัท) : &nbsp;{(percent > 0 ? (100 - percent) : 100 )}  &nbsp;%
                            </div>
                        </Col>
                        <Col span={24}>
                            <div style={{textAlign:'center',marginTop:10}}>
                                <button onClick={this.handleClickCancel} style={{fontSize:13,borderRadius:3,backgroundColor:'#f4f4f4',cursor:'pointer',minWidth:100,color:'#F05B3B'}} >
                                    คืนค่า
                                </button>
                                &nbsp;
                                <button onClick={this.handleClickSave} style={{fontSize:13,borderRadius:3,backgroundColor:'#f4f4f4',cursor:'pointer',minWidth:100,color:'#31B42F'}} >
                                    บันทึก
                                </button>
                                &nbsp;
                            </div>
                        </Col>
                    </Row>
                )}
            </div>
        )
    }

}

const mapStateToProps = state => {
    return { auth: state.auth }
}

export default connect( mapStateToProps )(ManageLoseWin)