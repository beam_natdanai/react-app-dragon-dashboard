import React, { Component } from "react"
import { Redirect, Route, Switch, } from 'react-router-dom'
import { connect } from 'react-redux'
import { Layout, Menu, Icon } from 'antd'
import { Row, Col, Transfer } from 'antd'
import routes from '../routers/router'
import { Service } from '../services/auth' 
import { Service as ServiceMember } from '../services/member' 
import { isEmpty } from '../lib/utils'
import Session, { getItem , clear } from '../lib/session'
import authAction from '../redux/auth/authRedux'
import Sidebar from './sidebar'
import { BaseMediaUrl } from '../config/configUrl'
import { Modal, Button } from 'antd'
import myStyle from './main_style'
import { check_is_english  } from '../lib/utils' 
// import { ModalConfirm } from '../components/modalConfirm'
import { mainMenu } from './dataMain'

const confirm = Modal.confirm

const { Header, Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;

class Main extends Component {

  constructor(props) {
    super(props)
    this.state = {
      dataEcditPass:undefined,
      inputEditPass:null,
      visibleEditpass: false,
      collapsed: false,
      member_name:undefined,
      member_priority:undefined,
      member_status:undefined,
      special_priority:{
        priority:undefined,
        parentId:undefined
      }
    }
  }

  componentDidMount() {
      if ( isEmpty( getItem( Session.header ) ) ) {
          this.props.history.push( 'login' )
      }else{
          let dataUser = getItem( Session.header )
          this.props.requestSetdata(dataUser)
          // set supersenior
          let member_priority = undefined
          if(dataUser.member.member_priority === 'super senior'){
              member_priority = 'supersenior'

              this.setState({
                member_name : dataUser.member.member_id,
                member_priority : member_priority,
                member_status : dataUser.member.member_status
              })
          }
          else if(dataUser.member.member_priority === 'watcher'){
            this.getPriolityById(dataUser.token,dataUser.member.member_grant).then((res)=>{
                if(res.status){

                  if(res.data.member_priority === 'super senior'){
                    member_priority = 'supersenior'
                  }else{
                    member_priority = res.data.member_priority
                  }

                  let special_priority = this.state.special_priority
                  special_priority.priority = 'watcher'
                  special_priority.parentId = dataUser.member.member_grant
                  this.setState({
                    member_name : dataUser.member.member_id,
                    member_priority : member_priority,
                    member_status : dataUser.member.member_status,
                    special_priority:special_priority
                  })
                }
            })
          }else{
            member_priority = dataUser.member.member_priority
            this.setState({
              member_name : dataUser.member.member_id,
              member_priority : member_priority,
              member_status : dataUser.member.member_status
            })
          }

      }
  }

  componentDidUpdate( preProps ){
    if ( preProps.auth.token !== this.props.auth.token ) {
        if(this.props.auth.token === null){
          this.props.history.push('login')
        }
    }
  }

  getPriolityById = (token,_id) => {
    return new Promise((resolve, reject) => {
      ServiceMember.getById(token,_id).then((res)=>{
          if(res.status){
            resolve({status:true,data : res.data.data})
          }else{
            resolve({status:false})
          }
      })
    }); 
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  clearEditPassCallback = () => {
    document.getElementById('inputEditPass').value = ""
    this.setState({visibleEditpass : false,inputEditPass : null})
  } 

  modalConfirmEditPassCallback = (_status) => {
    if(_status === 'Cancel'){ 
        this.clearEditPassCallback()
    }
    else{
        if(this.state.inputEditPass === "" || this.state.inputEditPass === null){
            alert("กรุณากรอกรหัสผ่านด้วยค่ะ")
        }else{
            ServiceMember.update(this.props.auth.token,this.props.auth.member._id,{member_pass : this.state.inputEditPass}).then((res,err) => {
                if(res.status){
                    alert("เปลี่ยนรหัสผ่านดเรียบร้อยค่ะ")
                    this.clearEditPassCallback()
                }else{
                    alert('เกิดข้อผิดพลาด กรุณาลองอีกครั้งค่ะ')
                }
            })
        }
    }
}

  handleChangePass = (event) => {

    if(check_is_english(event.target.value)){
        this.setState({
            inputEditPass : event.target.value,
        });
    }else{
        document.getElementById('inputEditPass').value = ""
        this.setState({
            inputEditPass : event.target.value,
        });
    }

  }

  funcUpdatePassword = () => {
    this.setState({visibleEditpass : true})
  }

  funcLogout = () => {
    Service.logout(this.props.auth.token).then((res,err) => {
      if(res.status){
          clear()
          alert('ออกจากระบบ')
          this.props.requestLogout()
          window.document.location.reload()
      }
    })

  }

  callbackSidebar = (_action) =>{

    const _this = this

    if(_action === 'logout'){
      confirm({
        title: " ออกจากระบบ ",
        content: ' ',
        okText: 'ตกลง',
        okType: 'primary',
        cancelText: 'ไม่',
        cancelType:'danger',
        onOk(){
          _this.funcLogout()
        },
        onCancel() {
          console.log('Cancel');
        },
      });
    }

  }

  handleClickMenu = (_key) => {

    if(this.props.history.location.pathname.substring(1) !== _key){
      this.props.history.push(_key)
    }
      
  }

  isSuperSenior = (_type) => {
    if(_type === 'supersenior'){
      return true
    }else{
      return false
    }
  }

  render() {

    const { menus } = this.props
    const { member_name , member_priority , member_status , visibleEditpass , special_priority } = this.state

    return (
      <Layout style={{minHeight:'100vh'}}>
        {/* <ModalConfirm title="แก้ไขรหัสผ่าน" buttonOk="บันทึก" buttonCancel="ยกเลิก" callback={this.modalConfirmEditPassCallback} show={visibleEditpass} >
            <form id="formTopupCredit">
                <Row>
                    <Col style={{textAlign:'center'}} span={24}>
                        รหัสผ่าน : <input maxLength={16} onChange={this.handleChangePass} id="inputEditPass" style={{width:90}} /> 
                    </Col>
                </Row>
            </form>
        </ModalConfirm> */}
        <Sider trigger={null}  collapsible collapsed={this.state.collapsed}>
          <div style={{paddingLeft:5,paddingRight:5,paddingTop:15,paddingBottom:10,height:81,textAlign:'center',color:'#fff'}}>
             {`ผู้ใช้ : ${member_name}`}
             {/* {`ผู้ใช้ : ${member_name} ( ${member_priority} ) `} */}
          </div>

          {
            (member_priority !== undefined && member_priority === 'supersenior' ? 
                  <div style={myStyle.menuFrame}>
                      <div style={myStyle.titleMenu}> จัดการ </div>
                      <div style={myStyle.SubMenu}>
                          {
                              mainMenu.menus.manage.priority[member_priority].map((row,index)=>{
                                return <p key={`menu_member${index}`} style={myStyle.menu} onClick={()=>{this.handleClickMenu(row.key)}}><Icon style={myStyle.arrowIcon} type="caret-right" /> {row.textTH} </p>
                              })
                          }
                      </div>
                  </div>
            : null )
          }

          <div style={myStyle.menuFrame}>
              <div style={myStyle.titleMenu}> ข้อมูลสมาชิก </div>
              <div style={myStyle.SubMenu}>
                  {
                    (member_priority !== undefined ? 
                      mainMenu.menus.member.priority[member_priority].map((row,index)=>{
                        return <p key={`menu_member${index}`} style={myStyle.menu} onClick={()=>{this.handleClickMenu(row.key)}}><Icon style={myStyle.arrowIcon} type="caret-right" /> {row.textTH} </p>
                      })
                    : null )
                  }
              </div>
          </div>

          <div style={myStyle.menuFrame}>
              <div style={myStyle.titleMenu}> รายงาน </div>
              <div style={myStyle.SubMenu}>
                  {
                    (member_priority !== undefined ? 
                      mainMenu.menus.report.priority[member_priority].map((row,index)=>{
                        return <p key={`menu_report${index}`} style={myStyle.menu} onClick={()=>{this.handleClickMenu(row.key)}}><Icon style={myStyle.arrowIcon} type="caret-right" /> {row.textTH} </p>
                      })
                    : null )
                  }
              </div>
          </div>

          <div style={myStyle.menuFrame}>
              <div style={myStyle.titleMenu}> ระบบ </div>
              <div style={myStyle.SubMenu}>
                  <p key={`menu_update_password`} style={myStyle.menu} onClick={()=>{this.funcUpdatePassword()}}><Icon style={myStyle.arrowIcon} type="caret-right" /> แก้ไขรหัสผ่าน </p>
              </div>
              <div style={myStyle.SubMenu}>
                  <p key={`menu_logout`} style={myStyle.menu} onClick={()=>{this.funcLogout()}}><Icon style={myStyle.arrowIcon} type="caret-right" /> ออกจากระบบ </p>
              </div>
          </div>
          
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
            <Icon
              className="trigger"
              style={{paddingLeft:20}}
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
          </Header>
          <Content style={{margin: '24px 16px',padding: 24,background: '#fff',minHeight: 280}}>
              <Switch>
                {
                  routes.map( ( route, idx ) => {
                    return route.component
                      ? ( <Route key={idx} path={route.path} exact={route.exact} name={route.name} render={props => ( <route.component special_priority={special_priority} priority={member_priority} {...props}/> )} /> )
                      : ( null );
                  } )
                }
              </Switch>
          </Content>
          <Footer style={{
              textAlign: 'center'
            }}>
            <strong>Dragon Game &copy; Dashboard </strong>
          </Footer>
        </Layout>
      </Layout> 
    );
  }

}

const mapStateToProps = state => {
  return { auth: state.auth, menus: state.menus }
}

const mapDispatchToProps = dispatch => {
  return {
    requestLogout: () => {
      dispatch( authAction.requestLogout() )
    },
    requestSetdata: ( _dataObj ) => {
      dispatch( authAction.loginSuccess( _dataObj ) )
    }
  }
}
export default connect( mapStateToProps, mapDispatchToProps )( Main );
