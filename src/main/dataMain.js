
const memberAllLevel = {
    supersenior:{
        key:'supersenior',
        textEn:'Supersenior',
        textTH:'ซูปเปอร์ซีเนียร์'
    },
    senior:{
        key:'senior',
        textEn:'Senior',
        textTH:'ซีเนียร์'
    },
    master:{
        key:'master',
        textEn:'Master',
        textTH:'มาสเตอร์'
    },
    agent:{
        key:'agent',
        textEn:'Agent',
        textTH:'เอเย่นต์'
    },
    member:{
        key:'member',
        textEn:'member',
        textTH:'สมาชิก'
    },
    watcher:{
        key:'watcher',
        textEn:'Watcher',
        textTH:'ผู้ช่วย'
    }
}

const reportAll = {
    rpt_win_lose:{
        key:'rpt_win_lose',
        textEn:'report win / lose',
        textTH:'แพ้ ชนะ (รายละเอียด)'
    },
    credit_transaction:{
        key:'rpt_credit_transaction',
        textEn:'credit transaction report',
        textTH:'รายงาน เครดิต'
    }
}

const manageAll = {
    manage_lose_win:{
        key:'manageLoseWin',
        textEn:'manage win / lose',
        textTH:'โอกาส แพ้ ชนะ'
    },
}

const dataObject = {

    menus:{
        member:{
            priority:{
                supersenior:[
                    memberAllLevel.senior,
                    memberAllLevel.watcher
                ],
                senior:[
                    memberAllLevel.master,
                    memberAllLevel.agent,
                    memberAllLevel.member,
                    memberAllLevel.watcher
                ],
                master:[
                    memberAllLevel.agent,
                    memberAllLevel.member,
                    memberAllLevel.watcher
                ],
                agent:[
                    memberAllLevel.member,
                    memberAllLevel.watcher
                ],
                watcher:[]
            }
        },
        report:{
            priority:{
                supersenior:[
                    reportAll.rpt_win_lose,
                    reportAll.credit_transaction
                ],
                senior:[
                    reportAll.rpt_win_lose,
                    reportAll.credit_transaction
                ],
                master:[
                    reportAll.rpt_win_lose,
                    reportAll.credit_transaction
                ],
                agent:[
                    reportAll.rpt_win_lose,
                    reportAll.credit_transaction
                ],
                watcher:[
                    reportAll.rpt_win_lose,
                    reportAll.credit_transaction
                ]
            },
        },
        manage:{
            priority:{
                supersenior:[
                    manageAll.manage_lose_win
                ],
                senior:[],
                master:[],
                agent:[]
            }
        }
        
    }

}

export const mainMenu = dataObject