const myStyle = {

    menuFrame:{
        margin:8,
        borderRadius:'6px',
        
    },
    SubMenu:{
        backgroundColor:'#454f59',
        borderRadius:'4px',
        margin:8,
        fontWeight:400,
        color:'#DFDFDF',
        padding:6,
        fontSize:'13px'
    },
    menu:{
        margin:0,
        cursor: 'pointer'
    },
    arrowIcon:{
        color:'#f98b8b'
    },
    titleMenu:{
        color:'#fff',
        fontWeight:600,
        fontSize:'15px'
    },


}

export default  myStyle