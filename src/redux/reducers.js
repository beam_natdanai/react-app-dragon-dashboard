import { applyMiddleware, combineReducers, compose, createStore, } from "redux"
import createSagaMiddleware from "redux-saga"
import { reducer as auth } from '../redux/auth/authRedux'

export const reducers = combineReducers( {
  auth
})
