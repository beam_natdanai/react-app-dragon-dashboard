import { createActions, createReducer } from "reduxsauce"
import * as Immutable from "seamless-immutable"

const tag = '[SAGA_AUTH_REDUX]'

const functionDefinitions = {
    loginSuccess: ["loginResponse"],
    requestLogout: null,
}

const { Types, Creators } = createActions(functionDefinitions);

export const ActionType = Types;
export default Creators;

const LOGOUT_STATE = {
    member:null,
    token: null
};
  
export const InitState = { ...LOGOUT_STATE };
export const INITIAL_STATE = Immutable(InitState);

export const loginSuccess = (state, action) => {
    if(action.loginResponse.key === undefined){
        return state.merge({
          loginFetching: false,
          token: action.loginResponse.token,
          member:action.loginResponse.member
        });
    }else{
          return state.merge({
            loginFetching: false,
            token: null,
            member:null
          });
    }

};

export const requestLogout = state => {
    return state.merge(LOGOUT_STATE);
};
  
export const reducer = createReducer(INITIAL_STATE, {
    [Types.LOGIN_SUCCESS]: loginSuccess,
    [Types.REQUEST_LOGOUT]: requestLogout
})

