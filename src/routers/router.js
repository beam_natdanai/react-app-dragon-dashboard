import React from 'react'
// import { Main } from '../main'
// import { Member } from '../containers/member'
import { Master } from '../containers/master'
// import { Agent } from '../containers/agent'
import { Senior } from '../containers/senior'
// import { Watcher } from '../containers/watcher'
// import { Rpt_win_lose } from '../containers/rpt_win_lose'
// import { ManageLoseWin } from '../containers/manageLoseWin'
// import { Rpt_credit_transaction } from '../containers/rpt_credit_transaction'

const routes = [
  // {
  //   path: '/',
  //   exact: true,
  //   name: 'main',
  //   component: Main,
  // },
  // {
  //   path: '/member',
  //   exact: true,
  //   name: 'member',
  //   component: Member,
  // },
  {
    path: '/master/',
    exact: true,
    name: 'master',
    component: Master,
  },
  // {
  //   path: '/agent/',
  //   exact: true,
  //   name: 'agent',
  //   component: Agent,
  // },
  {
    path: '/senior/',
    exact: true,
    name: 'senior',
    component: Senior,
  },
  // {
  //   path: '/rpt_win_lose/',
  //   exact: true,
  //   name: 'rpt_win_lose',
  //   component: Rpt_win_lose,
  // },
  // {
  //   path: '/manageLoseWin/',
  //   exact: true,
  //   name: 'manageLoseWin',
  //   component: ManageLoseWin,
  // },
  // {
  //   path: '/watcher/',
  //   exact: true,
  //   name: 'watcher',
  //   component: Watcher,
  // },
  // {
  //   path: '/rpt_credit_transaction/',
  //   exact: true,
  //   name: 'rpt_credit_transaction',
  //   component: Rpt_credit_transaction,
  // }
]


export default routes;
