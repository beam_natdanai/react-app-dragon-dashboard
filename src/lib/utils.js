export const isEmpty = (obj) =>  {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

export const validateEmail = (email) => {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

export const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export const subLengthString = (dataStr,length) => {

    if(dataStr !== null){
         if(dataStr.length > length){
             return dataStr.substring(0,length)+"..."
         }else{
             return dataStr.substring(0,length)
         }
    }

}

export const fillter = async (array,_priority) => {
    let newArray = array.filter((item,index) => {
        if ( item.priority === _priority){
            return item
        }
    });

    const result = await newArray
    return result
}

export const  check_is_english = (str) => {
    return /^[A-Za-z\d]+$/.test(str);
}

export const getDataNow = () => {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth()+1; //January is 0!
    let yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    }

    if(mm<10) {
        mm = '0'+mm
    }

    return today = yyyy + '-' + mm + '-' + dd ;
}

export const formatDate = (_date) => {
    return _date.substring(0,10) +" "+ _date.substring(11,19)
}
