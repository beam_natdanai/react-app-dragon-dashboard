import axios from 'axios'
import { BaseUrl } from '../config/configUrl'
import { fillter } from '../lib/utils'

const routeName = 'creditTransaction/'

export const Service  = {

    add : (_token,_data) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'post',
                url:(BaseUrl + routeName),
                data:_data,
                headers:{'Authorization': _token}
            }).then((response) => {
                resolve({status:true,data:response})
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        });     
    }

}
