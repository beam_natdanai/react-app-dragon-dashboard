import axios from 'axios'
import { BaseUrl } from '../config/configUrl'

const routeName = 'member/';

export const Service  = {

    login : (_uid,_pass) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'post',
                url:(BaseUrl + routeName)+'login/',
                data:({member_id:_uid,member_pass:_pass})
            }).then((response) => {
                resolve({status:true,data:response})
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        });
    },

    logout : (_token) => {

        return new Promise((resolve, reject) => {
            axios({
                method:'post',
                url:(BaseUrl + routeName)+'logout/',
                headers:{'Authorization': _token}
            }).then((response) => {
                resolve({status:true,data:response})
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        });     
    
    },

}
