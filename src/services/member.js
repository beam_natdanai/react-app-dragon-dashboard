import axios from 'axios'
import { BaseUrl } from '../config/configUrl'
import { fillter } from '../lib/utils'

const routeName = 'member/'
const routeName2 = 'memberTransaction/'

export const Service  = {

    add : (_token,_data) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'post',
                url:(BaseUrl + routeName),
                data:_data,
                headers:{'Authorization': _token}
            }).then((response) => {
                resolve({status:true,data:response})
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        });     
    },

    addChild : (_token,_data) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'post',
                url:(BaseUrl + routeName2),
                data:_data,
                headers:{'Authorization': _token}
            }).then((response) => {
                resolve({status:true,data:response})
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        });     
    },

    getByIdParents : (_token,_id,_priority) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'get',
                url:(BaseUrl + routeName2),
                headers:{'Authorization': _token}
            }).then( async (response) => {
                const dataFillter = await fillter(response.data,_priority)
                resolve({status:true,data:dataFillter})
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        }); 
    },

    getByIdParentsForReport : (_token,_id) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'get',
                url:(BaseUrl + routeName2),
                headers:{'Authorization': _token}
            }).then( async (response) => {
                resolve({status:true,data:response.data})
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        }); 
    },

    update : (_token,_id,_data) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'patch',
                url:(BaseUrl + routeName)+_id,
                data:_data,
                headers:{'Authorization': _token}
            }).then((response) => {
                resolve({status:true,data:response})
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        }); 
    },

    getCredit : (_token) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'get',
                url:(BaseUrl + routeName),
                data:{obj:'member_credit'},
                headers:{'Authorization': _token}
            }).then((response) => {
                resolve({status:true , data:{credit : response.data.member_credit} })
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        });     
    },

    getCreditUser : (_token,_id) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'get',
                url:(BaseUrl + routeName + _id),
                data:{obj:'member_credit'},
                headers:{'Authorization': _token}
            }).then((response) => {
                resolve({status:true , data:{credit : response.data.member_credit} })
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        });     
    },

    updateChanceOfWinning : (_token,_id,_data) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'patch',
                url:(BaseUrl + routeName)+_id,
                data:{member_win_rate:_data},
                headers:{'Authorization': _token}
            }).then((response) => {
                resolve({status:true,data:response})
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        }); 
    },

    getChanceOfWinning : (_token) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'get',
                url:(BaseUrl + routeName),
                headers:{'Authorization': _token}
            }).then((response) => {
                resolve({status:true , data:{percent : response.data.member_win_rate} })
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        });     
    },

    checkPass : (_token,pass) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'post',
                url:(BaseUrl + routeName) + 'check/password',
                headers:{'Authorization': _token},
                data:{password : pass}
            }).then((response) => {
                resolve({status:true , data:response})
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        });   
    },

    getById : (_token,_id) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'get',
                url:(BaseUrl + routeName + _id),
                headers:{'Authorization': _token}
            }).then( async (response) => {
                resolve({status:true,data:response})
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        }); 
    },

}
