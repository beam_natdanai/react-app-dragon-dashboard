import axios from 'axios'
import { BaseUrl } from '../config/configUrl'
import { fillter } from '../lib/utils'

const routeName = 'report/me/'
const routeName2 = 'report/history-credit/'

export const Service  = {

    getReportWinAndLose : (_token,_data) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'post',
                url:(BaseUrl + routeName),
                data:_data,
                headers:{'Authorization': _token}
            }).then((response) => {
                resolve({status:true,data:response})
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        });     
    },

    getReportCreditTransaction : (_token,_id,_data) => {
        return new Promise((resolve, reject) => {
            axios({
                method:'post',
                url:(BaseUrl + routeName2 + _id),
                headers:{'Authorization': _token},
                data:_data
            }).then((response) => {
                resolve({status:true,data:response})
            }).catch((err) => {
                resolve({status:false,err:err})
            })
        });     
    }

}
