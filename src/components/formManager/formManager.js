import React, { Component } from 'react'
import { Form, Icon , Tooltip , Input, Button , Checkbox , Switch , Radio , InputNumber , Modal , Row , Col , Spin } from 'antd'
import { numberWithCommas } from '../../lib/utils' 

const radioStyle = {
    height: '30px',
    lineHeight: '30px',
}

class ModalForm extends Component {
    
    constructor(props){
        super(props)

        this.state = {
                status:{
                    Cancel:'Cancel',
                    Add:'Add'
                },
                visible: props.visible,
                loading: props.loading,
                title: props.title,
                percent: props.percent,
                credit: props.credit,
                form: {
                    credit: 0,
                    percent: 0,
                    pay_choice: null,
                    promise_pay: null
                },
                dataDefault: props.dataDefault,

        };
    }

    componentDidUpdate(preProps){
        if(preProps.show !== this.props.show){
            this.setState({visible:this.props.show})
        }

        if(preProps.dataDefault.member_credit !== this.state.form.credit && preProps.dataDefault.percent !== this.state.form.percent){
            const obj = {
                credit: preProps.dataDefault.member_credit,
                percent: preProps.dataDefault.percent,
                pay_choice: preProps.dataDefault.member_choice_pay,
                promise_pay: preProps.dataDefault.member_promise_pay
            }
            this.setState({form:obj})
        }

    }



    handleCancel = () => {

        let _form = this.state.form
        _form.credit = 0
        _form.percent = 0

        this.props.form.resetFields()
        this.setState({loading:true,form:_form})
        setTimeout(()=>{
            this.setState({loading:false})
            this.props.callback(this.state.status.Cancel,null)
        },500)

    }

    handleSubmit = ( e ) => {

        e.preventDefault();
        this.props.form.validateFields(( err, values ) => {

            if ( !err ) {
                this.props.callback(this.state.status.Add,values)
            }

        } );

    }

    handleChangCredit = (_value) => {
        let _form = this.state.form
        _form.credit = _value
        this.setState({form : _form})
    }

    handleChangPercent = (_value) => {
        let _form = this.state.form
        _form.percent = _value
        this.setState({form : _form})
    }

    handleChangInput = ( event , params ) => {

        let _form = this.state.form

        if(params === 'promise_pay'){
            _form[params] = event
            this.setState({form : _form})
        }else{
            _form[params] = event.target.value
            this.setState({form : _form})
        }
            
    }   

    render(){

        const { visible , loading , title , percent , credit , form , dataDefault } = this.state
        const { getFieldDecorator } = this.props.form

        return(
                    <Modal
                    width={760}
                    visible={visible}
                    title={title}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="bt-close" className="dg-button-danger" type="danger" onClick={this.handleCancel} >
                            ปิด
                        </Button>,
                        <Button key="bt-add" className="dg-button-primary" type="primary" onClick={this.handleSubmit} >
                            เพิ่ม
                        </Button>
                    ]}
                    >
                        <Spin tip="กำลังโหลด..." spinning={loading}>
                                {(dataDefault === undefined ?  
                                
                                    <Form className="manager-form">
                                        <Row>
                                            <Col xs={24} md={12} style={{marginBottom:0}} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={8}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >รหัสผู้ใช้ :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={14} md={16}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'username', {
                                                                rules: [
                                                                    {
                                                                    required: true,
                                                                    message: 'กรอกรหัสผู้ใช้'
                                                                    }
                                                                ],
                                                                } )(    

                                                                    <Input  
                                                                        style={{width:150}}
                                                                        maxLength={16} 
                                                                        addonBefore="dg"
                                                                        placeholder=" กรอกรหัสผู้ใช้ "
                                                                    /> 
                                                                )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} md={12} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={8}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >รหัสผ่าน :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={14} md={16}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'password', {
                                                                rules: [
                                                                    {
                                                                    required: true,
                                                                    message: 'กรอกรหัสผ่าน'
                                                                    }
                                                                ],
                                                                } )(    
                                                                        <Input.Password  
                                                                            style={{width:150}}
                                                                            maxLength={16} 
                                                                            placeholder=" กรอกรหัสผ่าน"
                                                                        /> 
                                                                    )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} md={12} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={8}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >เครดิตจำกัด :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={8} md={8}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'credit', {
                                                                rules: [
                                                                    {
                                                                        required: true,
                                                                        message: 'กรอกเครดิตจำกัด :'
                                                                    }
                                                                ],
                                                                initialValue: form.credit
                                                                } )(    
                                                                        <InputNumber
                                                                            min={1}
                                                                            max={1000000}
                                                                            onChange={this.handleChangCredit}
                                                                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                                                            style={{width:115}}
                                                                            placeholder="เครดิตจำกัด"
                                                                        />
                                                                    )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                    <Col xs={6} md={8}>
                                                        <div className="ant-form-item-control">
                                                            {(credit === undefined ? null : `สูงสุด = ${numberWithCommas((credit - form.credit))}` )}
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} md={12} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={8}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >ชื่อ ติดต่อ :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={14} md={16}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'contact', {
                                                                rules: [
                                                                    {
                                                                        required: true,
                                                                        message: 'กรอกชื่อติดต่อ :'
                                                                    }
                                                                ],
                                                                } )(    
                                                                        <Input  
                                                                            style={{width:150}}
                                                                            maxLength={20} 
                                                                            placeholder=" กรอกชื่อติดต่อ "
                                                                        /> 
                                                                    )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} md={12} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={8}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >สถาณะ  :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={14} md={16}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'status', {
                                                                initialValue: true,
                                                                valuePropName: "checked"
                                                                } )(    
                                                                    <Switch
                                                                        checkedChildren={<Icon type="check" />}
                                                                        unCheckedChildren={<Icon type="cross" />}
                                                                    />
                                                                    )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={4}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >ตัวเลือกการจ่าย  :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={14} md={20}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'pay_choice', {
                                                                rules: [{
                                                                    required: true, message: 'เลือกการจ่าย',
                                                                }],
                                                                valuePropName: "checked"
                                                                } )(    
                                                                    <Radio.Group >
                                                                        <Radio style={radioStyle} value="Power" > มีอำนาจ </Radio>
                                                                        <Radio style={radioStyle} value="Daily" > รายวัน </Radio>
                                                                        <Radio style={radioStyle} value="Weekly" > รายสัปดาห์ </Radio>
                                                                    </Radio.Group>
                                                                    )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={4}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" ></p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={14} md={20}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'promise_pay', {
                                                                rules: [{
                                                                    required: true, message: 'เลือกวัน',
                                                                }],
                                                                valuePropName: "checked"
                                                                } )(    
                                                                    <Checkbox.Group>
                                                                        <Checkbox value="Mon">จ.</Checkbox>
                                                                        <Checkbox value="Tue">อ.</Checkbox>
                                                                        <Checkbox value="Wed">พ.</Checkbox>
                                                                        <Checkbox value="Thu">พฤ.</Checkbox>
                                                                        <Checkbox value="Fri">ศ.</Checkbox>
                                                                        <Checkbox value="Sat">ส.</Checkbox>
                                                                        <Checkbox value="Sun">อา.</Checkbox>
                                                                    </Checkbox.Group>
                                                                )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={4}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >เปอร์เซ็น หุ้นส่วน :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={9} md={5}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'percent', {
                                                                rules: [{
                                                                    required: true, message: 'กรอกเปอร์เซ็น หุ้นส่วน',
                                                                }],
                                                                initialValue: form.percent,
                                                                } )(   
                                                                    <InputNumber
                                                                        min={0}
                                                                        max={100}
                                                                        onChange={this.handleChangPercent}
                                                                        formatter={value => `${value}%`}
                                                                        parser={value => value.replace('%', '')}
                                                                        style={{width:100}}
                                                                        placeholder="เปอร์เซ็น"
                                                                    />
                                                                )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                    <Col xs={5} md={15}>
                                                        <div className="ant-form-item-control">
                                                            {(percent === undefined ? '%' : `${numberWithCommas((percent - form.percent))} %` )}
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>

                                        </Row>
                                    </Form>

                                :  

                                    <Form className="manager-form">
                                        <Row>
                                            <Col xs={24} md={12} style={{marginBottom:0}} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={8}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >รหัสผู้ใช้ :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={14} md={16}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'username', {
                                                                rules: [
                                                                    {
                                                                        required: true,
                                                                        message: 'กรอกรหัสผู้ใช้'
                                                                    }
                                                                ],
                                                                initialValue: (dataDefault === undefined ? null : dataDefault.member_id  )
                                                                } )(    

                                                                    <Input  
                                                                        style={{width:150}}
                                                                        maxLength={16} 
                                                                        addonBefore="dg"
                                                                        placeholder=" กรอกรหัสผู้ใช้ "
                                                                    /> 
                                                                )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} md={12} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={8}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >รหัสผ่าน :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={14} md={16}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'password', {
                                                                rules: [
                                                                    {
                                                                    required: true,
                                                                    message: 'กรอกรหัสผ่าน'
                                                                    }
                                                                ],
                                                                initialValue: (dataDefault === undefined ? null : dataDefault.member_pass  )
                                                                } )(    
                                                                        <Input.Password  
                                                                            disabled
                                                                            style={{width:150}}
                                                                            maxLength={16} 
                                                                            placeholder=" กรอกรหัสผ่าน"
                                                                        /> 
                                                                    )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} md={12} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={8}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >เครดิตจำกัด :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={8} md={8}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'credit', {
                                                                rules: [
                                                                    {
                                                                        required: true,
                                                                        message: 'กรอกเครดิตจำกัด :'
                                                                    }
                                                                ],
                                                                initialValue: form.credit
                                                                } )(    
                                                                        <InputNumber
                                                                            disabled
                                                                            min={1}
                                                                            max={1000000}
                                                                            onChange={this.handleChangCredit}
                                                                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                                                            style={{width:115}}
                                                                            placeholder="เครดิตจำกัด"
                                                                        />
                                                                    )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                    <Col xs={6} md={8}>
                                                        <div className="ant-form-item-control">
                                                            {(credit === undefined ? null : `สูงสุด = ${numberWithCommas((credit - form.credit))}` )}
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} md={12} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={8}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >ชื่อ ติดต่อ :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={14} md={16}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'contact', {
                                                                rules: [
                                                                    {
                                                                        required: true,
                                                                        message: 'กรอกชื่อติดต่อ :'
                                                                    }
                                                                ],
                                                                initialValue: (dataDefault === undefined ? null : dataDefault.member_name  )
                                                                } )(    
                                                                        <Input  
                                                                            style={{width:150}}
                                                                            maxLength={20} 
                                                                            placeholder=" กรอกชื่อติดต่อ "
                                                                        /> 
                                                                    )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} md={12} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={8}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >สถาณะ  :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={14} md={16}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'status', {
                                                                initialValue: true,
                                                                valuePropName: "checked"
                                                                } )(    
                                                                    <Switch
                                                                        checkedChildren={<Icon type="check" />}
                                                                        unCheckedChildren={<Icon type="cross" />}
                                                                    />
                                                                    )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={4}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >ตัวเลือกการจ่าย  :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={14} md={20}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'pay_choice', {
                                                                rules: [{
                                                                    required: true, message: 'เลือกการจ่าย',
                                                                }],
                                                                initialValue: form.pay_choice,
                                                                valuePropName: "checked"
                                                                } )(    
                                                                    <Radio.Group value={form.pay_choice} onChange={(e)=>{this.handleChangInput(e,'pay_choice')}} >
                                                                        <Radio style={radioStyle} value="Power" > มีอำนาจ </Radio>
                                                                        <Radio style={radioStyle} value="Daily" > รายวัน </Radio>
                                                                        <Radio style={radioStyle} value="Weekly" > รายสัปดาห์ </Radio>
                                                                    </Radio.Group>
                                                                    )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={4}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" ></p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={14} md={20}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'promise_pay', {
                                                                rules: [{
                                                                    required: true, message: 'เลือกวัน',
                                                                }],
                                                                initialValue: form.promise_pay,
                                                                valuePropName: 'checked',
                                                                } )(    
                                                                    <Checkbox.Group value={form.promise_pay} onChange={(e)=>{this.handleChangInput(e,'promise_pay')}} >
                                                                        <Checkbox value={'Mon'}>จ.</Checkbox>
                                                                        <Checkbox value={'Tue'}>อ.</Checkbox>
                                                                        <Checkbox value={'Wed'}>พ.</Checkbox>
                                                                        <Checkbox value={'Thu'}>พฤ.</Checkbox>
                                                                        <Checkbox value={'Fri'}>ศ.</Checkbox>
                                                                        <Checkbox value={'Sat'}>ส.</Checkbox>
                                                                        <Checkbox value={'Sun'}>อา.</Checkbox>
                                                                    </Checkbox.Group>
                                                                )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={24} >
                                                <Row gutter={8} >
                                                    <Col xs={10} md={4}>
                                                        <div className="ant-form-item-control">
                                                            <p className="dg-text-form pull-right" >เปอร์เซ็น หุ้นส่วน :</p>
                                                        </div>
                                                    </Col>
                                                    <Col xs={9} md={5}>
                                                        <Form.Item>
                                                            {
                                                                getFieldDecorator( 'percent', {
                                                                rules: [{
                                                                    required: true, message: 'กรอกเปอร์เซ็น หุ้นส่วน',
                                                                }],
                                                                initialValue: form.percent,
                                                                } )(   
                                                                    <InputNumber
                                                                        disabled
                                                                        min={0}
                                                                        max={100}
                                                                        onChange={this.handleChangPercent}
                                                                        formatter={value => `${value}%`}
                                                                        parser={value => value.replace('%', '')}
                                                                        style={{width:100}}
                                                                        placeholder="เปอร์เซ็น"
                                                                    />
                                                                )
                                                            }
                                                        </Form.Item>
                                                    </Col>
                                                    <Col xs={5} md={15}>
                                                        <div className="ant-form-item-control">
                                                            {(percent === undefined ? '%' : `${numberWithCommas((percent - form.percent))} %` )}
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>

                                        </Row>
                                    </Form>
                                )}
                        </Spin>
                    </Modal>
        )

    }

  
}

const WrappedManagerForm = Form.create( { name: 'ManagerForm' } )( ModalForm );

export default WrappedManagerForm