import React, { Component } from 'react'
import { Row, Col } from 'antd'
import { Icon } from 'antd'
import '../pathTitle/pathTitle.css'


class PathTitle extends Component {

    constructor(props) {
        super(props)
        this.state = {
          
        }
    }

    componentDidMount() {
        
    }

    render(){

        const { textPath } = this.props

        return (
            <div>
                <Row>
                    <Col span={24}>
                        <p className="dg-title"><Icon className="dg-arrow-icon" type="caret-right" /> {textPath} </p>
                    </Col>
                </Row>
            </div>
        )
    }

}

export default PathTitle