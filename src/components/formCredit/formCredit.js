import React, { Component } from 'react'
import { Form, Icon , Tooltip , Input, Button , Checkbox , Switch , Radio , InputNumber , Modal , Row , Col , Spin } from 'antd'
import { numberWithCommas } from '../../lib/utils' 

const radioStyle = {
    height: '30px',
    lineHeight: '30px',
}

class ModalForm extends Component {
    
    constructor(props){
      super(props)
      this.state = {
            status:{
                Cancel:'Cancel',
                Add:'Add'
            },
            visible: props.visible,
            loading: props.loading,
            title: props.title,
            credit: props.credit,
            form: {
                credit:0
            }
      };
    }

    componentDidUpdate(preProps){
      if(preProps.show !== this.props.show){
        this.setState({visible:this.props.show})
      }
    }

    handleCancel = () => {

        let _form = this.state.form
        _form.credit = 0

        this.props.form.resetFields()
        this.setState({loading:true,form:_form})
        setTimeout(()=>{
            this.setState({loading:false})
            this.props.callback(this.state.status.Cancel,null)
        },500)

    }

    handleSubmit = ( e ) => {

        e.preventDefault();
        this.props.form.validateFields(( err, values ) => {

            if ( !err ) {
                this.props.callback(this.state.status.Add,values)
            }

        } );

    }

    handleChangCredit = (_value) => {
        let _form = this.state.form
        _form.credit = _value
        this.setState({form : _form})
    }

    render(){

        const { visible , loading , title , credit , form } = this.state
        const { getFieldDecorator } = this.props.form

        return(
                    <Modal
                    width={760}
                    visible={visible}
                    title={title}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="bt-close" className="dg-button-danger" type="danger" onClick={this.handleCancel} >
                            ปิด
                        </Button>,
                        <Button key="bt-add" className="dg-button-primary" type="primary" onClick={this.handleSubmit} >
                            บันทึก
                        </Button>
                    ]}
                    >
                        <Spin tip="กำลังโหลด..." spinning={loading}>
                            <Form className="manager-form">
                                <Row>

                                    <Col xs={24} >
                                        <Row gutter={8} >
                                            <Col xs={10} md={10}>
                                                <div className="ant-form-item-control">
                                                    <p className="dg-text-form pull-right" >เครดิต :</p>
                                                </div>
                                            </Col>
                                            <Col xs={14} md={5}>
                                                <Form.Item>
                                                    {
                                                        getFieldDecorator( 'credit', {
                                                        rules: [
                                                            {
                                                                required: true,
                                                                message: 'กรอกเครดิต :',
                                                            }
                                                        ],
                                                        initialValue: form.credit
                                                        } )(    
                                                                <InputNumber
                                                                    min={0}
                                                                    max={1000000}
                                                                    onChange={this.handleChangCredit}
                                                                    formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                                                    style={{width:115}}
                                                                    placeholder="กรอกเครดิต"
                                                                />
                                                            )
                                                    }
                                                </Form.Item>
                                            </Col>
                                            <Col sm={0} xs={{span:14,offset:10}}>
                                                <div className="ant-form-item-control">
                                                    {(credit === undefined ? null : `สูงสุด = ${numberWithCommas((credit - form.credit))}` )}
                                                </div>
                                            </Col>
                                            <Col sm={{ span: 9}} xs={0}>
                                                <div className="ant-form-item-control">
                                                    {(credit === undefined ? null : `สูงสุด = ${numberWithCommas((credit - form.credit))}` )}
                                                </div>
                                            </Col>
                                        </Row>
                                    </Col>

                                </Row>
                            </Form>
                        </Spin>
                    </Modal>
        )

    }

  
}

const WrappedManagerForm = Form.create( { name: 'ManagerForm' } )( ModalForm );

export default WrappedManagerForm