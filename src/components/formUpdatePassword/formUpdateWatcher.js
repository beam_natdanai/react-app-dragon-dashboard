import React, { Component } from 'react'
import { Form, Icon , Tooltip , Input, Button , Checkbox , Switch , Radio , InputNumber , Modal , Row , Col , Spin } from 'antd'
import { numberWithCommas } from '../../lib/utils' 

const radioStyle = {
    height: '30px',
    lineHeight: '30px',
}

class ModalForm extends Component {
    
    constructor(props){
      super(props)
      this.state = {
            status:{
                Cancel:'Cancel',
                Add:'Add'
            },
            visible: props.visible,
            loading: props.loading,
            title: props.title
      };
    }

    componentDidUpdate(preProps){
      if(preProps.show !== this.props.show){
        this.setState({visible:this.props.show})
      }
    }

    handleCancel = () => {

        this.props.form.resetFields()
        this.setState({loading:true})
        setTimeout(()=>{
            this.setState({loading:false})
            this.props.callback(this.state.status.Cancel,null)
        },500)

    }

    handleSubmit = ( e ) => {

        e.preventDefault();
        this.props.form.validateFields(( err, values ) => {

            if ( !err ) {
                this.props.callback(this.state.status.Add,values)
            }

        } );

    }

    render(){

        const { visible , loading , title } = this.state
        const { getFieldDecorator } = this.props.form

        return(
                    <Modal
                    width={760}
                    visible={visible}
                    title={title}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="bt-close" className="dg-button-danger" type="danger" onClick={this.handleCancel} >
                            ปิด
                        </Button>,
                        <Button key="bt-add" className="dg-button-primary" type="primary" onClick={this.handleSubmit} >
                            บันทึก
                        </Button>
                    ]}
                    >
                        <Spin tip="กำลังโหลด..." spinning={loading}>
                            <Form className="manager-form">
                                <Row>

                                    <Col xs={24} md={24} >
                                        <Row gutter={8} >
                                            <Col xs={10} md={10}>
                                                <div className="ant-form-item-control">
                                                    <p className="dg-text-form pull-right" >รหัสผ่าน :</p>
                                                </div>
                                            </Col>
                                            <Col xs={14} md={14}>
                                                <Form.Item>
                                                    {
                                                        getFieldDecorator( 'password', {
                                                        rules: [
                                                            {
                                                            required: true,
                                                            message: 'กรอกรหัสผ่าน'
                                                            }
                                                        ],
                                                        } )(    
                                                                <Input.Password  
                                                                    style={{width:150}}
                                                                    maxLength={16} 
                                                                    placeholder=" กรอกรหัสผ่าน"
                                                                /> 
                                                            )
                                                    }
                                                </Form.Item>
                                            </Col>
                                        </Row>
                                    </Col>

                                </Row>
                            </Form>
                        </Spin>
                    </Modal>
        )

    }

  
}

const WrappedManagerForm = Form.create( { name: 'ManagerForm' } )( ModalForm );

export default WrappedManagerForm